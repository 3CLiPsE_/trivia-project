﻿using System.Collections.Generic;
using MessagePack;

namespace MessagePacks.Request
{
    [MessagePackObject(true)]
    public class Login
    {
        [Key(0)]
        public string Username { get; set; }
        [Key(1)]
        public string Password { get; set; }
    }
    [MessagePackObject(true)]
    public class SignUp
    {
        [Key(0)]
        public string Username { get; set; }
        [Key(1)]
        public string Password { get; set; }
        [Key(2)]
        public string Email { get; set; }
    }
    [MessagePackObject(true)]
    public class JoinRoom
    {
        [Key(0)]
        public uint Room_id { get; set; }
    }
    [MessagePackObject(true)]
    public class GetPlayers
    {
        [Key(0)]
        public uint Room_id { get; set; }
    }

    [MessagePackObject(true)]
    public class CreateRoom
    {
        [Key(0)]
        public string Name { get; set; }
        [Key(1)]
        public uint Max_users { get; set; }
        [Key(2)]
        public uint Questions_count { get; set; }
        [Key(3)]
        public uint Time { get; set; }
    }
    [MessagePackObject(true)]
    public class SubmitAnswer
    {
        [Key(0)]
        public int Answer_Id { get; set; }
    }
}

namespace MessagePacks.Response
{
    [MessagePackObject(true)]
    public class NormalResponse
    {
        [Key(0)]
        public byte Status { get; set; }
    }
    [MessagePackObject(true)]
    public class ErrorResponse : NormalResponse
    {
        [Key(1)]
        public string Message { get; set; }
    }

    [MessagePackObject(true)]
    public class HighscoreTable : NormalResponse
    {
        [Key(1)]
        public Dictionary<string, int> Highscores { get; set; }
    }
    [MessagePackObject(true)]
    public class Statistics : NormalResponse
    {
        [Key(1)]
        public uint Correct { get; set; }
        [Key(2)]
        public uint Wrong { get; set; }
        [Key(3)]
        public double Avg_time { get; set; }
    }
    [MessagePackObject(true)]
    public class Room
    {
        [Key(0)]
        public uint Id { get; set; }
        [Key(1)]
        public string Name { get; set; }
        [Key(2)]
        public bool Active { get; set; }
        [Key(3)]
        public uint Question_time { get; set; }
        [Key(4)]
        public uint Max_players { get; set; }
        [Key(5)]
        public uint Question_num { get; set; }
    }
    [MessagePackObject(true)]
    public class RoomList : NormalResponse
    {
        [Key(1)]
        public Room[] Rooms { get; set; }
    }

    [MessagePackObject(true)]
    public class PlayerList : NormalResponse
    {
        [Key(1)]
        public string[] Players { get; set; }
    }

    [MessagePackObject(true)]
    public class CreateRoom : NormalResponse
    {
        [Key(1)]
        public uint Id { get; set; }
    }

    [MessagePackObject(true)]
    public class RoomState : NormalResponse
    {
        public uint Question_time { get; set; }
        public bool Active { get; set; }
        public uint Question_num { get; set; }
        public string[] Players { get; set; }
    }
    [MessagePackObject(true)]
    public class GetAnswer : NormalResponse
    {
        [Key(0)]
        public int Correct_answer_index { get; set; }
    }
    [MessagePackObject(true)]
    public class GetQuestion : NormalResponse
    {
        [Key(1)]
        public string Question { get; set; }
        [Key(2)]
        public string[] Answers { get; set; }
    }
    [MessagePackObject(true)]
    public class GameResult
    {
        [Key(0)]
        public string Username { get; set; }
        [Key(1)]
        public uint Correct { get; set; }
        [Key(2)]
        public uint Wrong { get; set; }
        [Key(3)]
        public double Avg_time { get; set; }
    }
    [MessagePackObject(true)]
    public class GameResults : NormalResponse
    {
        [Key(1)]
        public GameResult[] Results { get; set; }
    }
}
