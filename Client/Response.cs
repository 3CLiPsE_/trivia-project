﻿namespace Client
{
    public class Response
    {
        public byte Code { get; set; }
        public byte[] Message { get; set; }
    }
}
