﻿using System;

namespace Client
{
    class TypeException : Exception
    {
        public TypeException(string message) :
            base(message)
        {

        }
        public byte Expected { get; set; }
        public byte Got { get; set; }
    }
}
