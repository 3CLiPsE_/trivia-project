﻿using System;
using System.Net.Sockets;
using System.Net;
using MessagePack;
using System.Threading;

namespace Client
{
    public class ClientCommunicator
    {
        public const string IP = "127.0.0.1";
        public const int PORT = 1337;

        readonly TcpClient sock;
        public bool Connected
        {
            get
            { return sock != null && sock.Connected; }
        }

        public ClientCommunicator()
        {
            sock = new TcpClient();
            sock.Connect(IP, PORT);
        }

        public void SendMessage<T>(byte code, T msgObject)
        {
            var stream = sock.GetStream();
            var msg = MessagePackSerializer.Serialize(msgObject);

            stream.Write(new byte[] { code }, 0, 1);
            stream.Flush();
            stream.Write(BitConverter.GetBytes(IPAddress.HostToNetworkOrder(msg.Length)), 0, sizeof(int));
            stream.Flush();
            stream.Write(msg, 0, msg.Length);
            stream.Flush();
        }

        public void SendMessage(byte code)
        {
            var stream = sock.GetStream();

            stream.Write(new byte[] { code }, 0, 1);
            stream.Flush();
            stream.Write(BitConverter.GetBytes(IPAddress.HostToNetworkOrder(0)), 0, sizeof(int));
            stream.Flush();
        }

        public MessagePacks.Response.NormalResponse GetMessage<T>(byte msgCode)
            where T : MessagePacks.Response.NormalResponse
        {
            Response response = GetMessage();

            if (response.Code != msgCode)
                throw new TypeException("Got Unexpected Code")
                {
                    Expected = msgCode,
                    Got = response.Code,
                };
            var deserialized = MessagePackSerializer.Deserialize<T>(response.Message);
            if (deserialized.Status == ProtocolCodes.REQUEST_UNSUCCESSFUL)
                return MessagePackSerializer.Deserialize<MessagePacks.Response.ErrorResponse>(response.Message);
            return deserialized;
        }
        public Response GetMessage()
        {
            var stream = sock.GetStream();

            int code = stream.ReadByte();

            byte[] buffer = new byte[sizeof(int)];
            if (stream.Read(buffer, 0, sizeof(int)) != sizeof(int))
                throw new Exception("Not enough bytes for length.");
            int len = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(buffer, 0));

            byte[] msg = null;

            if (len > 0)
            {
                msg = new byte[len];
                if (stream.Read(msg, 0, len) != len)
                    throw new Exception($"Not enough bytes for msg. expected {len} bytes");
            }

            return new Response { Code = (byte)code, Message = msg };
        }

        public Response GetMessage(CancellationToken token)
        {
            var stream = sock.GetStream();

            try
            {
                int code = stream.ReadByte(token);

                int len = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(stream.Read(sizeof(int), token), 0));

                byte[] msg = null;

                if (len > 0)
                {
                    msg = stream.Read(len, token);
                }

                return new Response { Code = (byte)code, Message = msg };
            }
            catch (OperationCanceledException)
            {
                return null;
            }
        }

        public static class ProtocolCodes
        {
            public const byte REQUEST_SUCCESSFUL = 200;
            public const byte REQUEST_UNSUCCESSFUL = 201;

            public const byte LOGIN_REQUEST_CODE = 24;
            public const byte SIGNUP_REQUEST_CODE = 160;
            public const byte SIGNOUT_REQUEST_CODE = 51;

            public const byte GET_PLAYER_STATISTICS_REQUEST_CODE = 52;
            public const byte GET_HIGHSCORES_REQUEST_CODE = 72;

            public const byte GET_ROOMS_REQUEST_CODE = 216;
            public const byte GET_PLAYERS_REQUEST_CODE = 39;
            public const byte JOIN_REQUEST_CODE = 77;
            public const byte CREATE_REQUEST_CODE = 90;
            public const byte LEAVE_ROOM_REQUEST_CODE = 158;
            public const byte GET_ROOM_STATE_REQUEST_CODE = 22;
            public const byte START_GAME_REQUEST_CODE = 217;
            public const byte CLOSE_ROOM_REQUEST_CODE = 116;

            public const byte GET_GAME_RESULTS_REQUEST_CODE = 102;
            public const byte GET_QUESTION_REQUEST_CODE = 230;
            public const byte LEAVE_GAME_REQUEST_CODE = 83;
            public const byte SUBMIT_ANSWER_REQUEST_CODE = 254;
            public const byte TIME_OUT_RESPONSE_CODE = 145;
        }

        ~ClientCommunicator()
        {
            sock.Close();
        }
    }
}
