﻿using System;
using System.Windows.Forms;

namespace Client.Forms.Login_Forms
{
    public partial class Sign_In : Form
    {
        protected readonly ClientCommunicator communicator;
        public Sign_In(ClientCommunicator communicator)
        {
            InitializeComponent();
            username.WaterMarkText = "Enter username...";
            password.WaterMarkText = "Enter password...";

            this.communicator = communicator;
        }
        public string Username
        {
            get { return username.Text; }
        }
        protected virtual void Confirm_Click(object sender, EventArgs e)
        {
            if (username.Text == "" ||
                password.Text == "")
            {
                MessageBox.Show("ERROR! Please enter a usename and password.");
                return;
            }

            var data = new MessagePacks.Request.Login
            {
                Username = username.Text,
                Password = password.Text,
            };

            try
            {
                communicator.SendMessage(ClientCommunicator.ProtocolCodes.LOGIN_REQUEST_CODE, data);
                var response = communicator.GetMessage<MessagePacks.Response.NormalResponse>(ClientCommunicator.ProtocolCodes.LOGIN_REQUEST_CODE);
                if (response.Status != ClientCommunicator.ProtocolCodes.REQUEST_SUCCESSFUL)
                {
                    MessageBox.Show($"Can't login: {((MessagePacks.Response.ErrorResponse)response).Message}");
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            ((Login)Owner).Username = username.Text;
            Close();
        }
    }
}
