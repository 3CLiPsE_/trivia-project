﻿using System;
using System.Windows.Forms;

namespace Client.Forms.Login_Forms
{
    public partial class Login : Form
    {
        readonly ClientCommunicator communicator;

        public Login(ClientCommunicator communicator)
        {
            InitializeComponent();

            this.communicator = communicator;
        }

        public string Username
        {
            set { ((Menu.Menu)Owner).Username = value; }
        }

        private void Sign_in_Click(object sender, EventArgs e)
        {
            Sign_In sign_in = new Sign_In(communicator);
            Hide();

            sign_in.ShowDialog(this);
            Close();
        }

        private void Sign_up_Click(object sender, EventArgs e)
        {
            Sign_Up sign_up = new Sign_Up(communicator);
            Hide();

            sign_up.ShowDialog(this);
            Close();
        }
    }
}
