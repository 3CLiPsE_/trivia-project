﻿using System;
using System.Windows.Forms;

namespace Client.Forms.Login_Forms
{
    public partial class Sign_Up : Sign_In
    {
        public Sign_Up(ClientCommunicator communicator) : base(communicator)
        {
            InitializeComponent();
        }

        protected override void Confirm_Click(object sender, EventArgs e)
        {
            if (username.Text == "" ||
                password.Text == "" ||
                email.Text == "")
            {
                MessageBox.Show("ERROR! Please enter a usename and password.");
                return;
            }

            var data = new MessagePacks.Request.SignUp
            {
                Username = username.Text,
                Password = password.Text,
                Email = email.Text,
            };

            communicator.SendMessage(ClientCommunicator.ProtocolCodes.SIGNUP_REQUEST_CODE, data);
            try
            {
                var response = communicator.GetMessage<MessagePacks.Response.NormalResponse>(ClientCommunicator.ProtocolCodes.SIGNUP_REQUEST_CODE);
                if (response.Status == ClientCommunicator.ProtocolCodes.REQUEST_SUCCESSFUL)
                    ((Login)Owner).Username = username.Text;
                else
                {
                    MessageBox.Show($"Couldn't sign up: {((MessagePacks.Response.ErrorResponse)response).Message}");
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Close();
        }
    }
}
