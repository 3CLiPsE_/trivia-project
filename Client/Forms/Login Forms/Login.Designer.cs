﻿namespace Client.Forms.Login_Forms
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sign_up = new System.Windows.Forms.Button();
            this.sign_in = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // sign_up
            // 
            this.sign_up.Location = new System.Drawing.Point(128, 144);
            this.sign_up.Name = "sign_up";
            this.sign_up.Size = new System.Drawing.Size(184, 59);
            this.sign_up.TabIndex = 3;
            this.sign_up.Text = "Sign-up";
            this.sign_up.UseVisualStyleBackColor = true;
            this.sign_up.Click += new System.EventHandler(this.Sign_up_Click);
            // 
            // sign_in
            // 
            this.sign_in.Location = new System.Drawing.Point(128, 40);
            this.sign_in.Name = "sign_in";
            this.sign_in.Size = new System.Drawing.Size(184, 59);
            this.sign_in.TabIndex = 2;
            this.sign_in.Text = "Sign-in";
            this.sign_in.UseVisualStyleBackColor = true;
            this.sign_in.Click += new System.EventHandler(this.Sign_in_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 264);
            this.ControlBox = false;
            this.Controls.Add(this.sign_up);
            this.Controls.Add(this.sign_in);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Login";
            this.Text = "Login";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button sign_up;
        private System.Windows.Forms.Button sign_in;
    }
}