﻿namespace Client.Forms.Login_Forms
{
    partial class Sign_In
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.username = new System.Windows.Forms.WatermarkTextBox();
            this.password = new System.Windows.Forms.WatermarkTextBox();
            this.confirm = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // username
            // 
            this.username.Location = new System.Drawing.Point(216, 112);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(344, 22);
            this.username.TabIndex = 0;
            this.username.WaterMarkColor = System.Drawing.Color.Gray;
            this.username.WaterMarkText = "Water Mark";
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(216, 160);
            this.password.Name = "password";
            this.password.PasswordChar = '*';
            this.password.Size = new System.Drawing.Size(344, 22);
            this.password.TabIndex = 1;
            this.password.WaterMarkColor = System.Drawing.Color.Gray;
            this.password.WaterMarkText = "Water Mark";
            // 
            // confirm
            // 
            this.confirm.Location = new System.Drawing.Point(320, 272);
            this.confirm.Name = "confirm";
            this.confirm.Size = new System.Drawing.Size(144, 64);
            this.confirm.TabIndex = 2;
            this.confirm.Text = "OK";
            this.confirm.UseVisualStyleBackColor = true;
            this.confirm.Click += new System.EventHandler(this.Confirm_Click);
            // 
            // Sign_In
            // 
            this.AcceptButton = this.confirm;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.confirm);
            this.Controls.Add(this.password);
            this.Controls.Add(this.username);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Sign_In";
            this.Text = "signin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected System.Windows.Forms.WatermarkTextBox username;
        protected System.Windows.Forms.WatermarkTextBox password;
        private System.Windows.Forms.Button confirm;
    }
}