﻿using System;
using System.Windows.Forms;
using System.Linq;

namespace Client.Forms.Menu
{
    public partial class Highscores : Form
    {
        readonly MessagePacks.Response.HighscoreTable table;

        public Highscores(MessagePacks.Response.HighscoreTable table)
        {
            InitializeComponent();
            this.table = table;
            Load += LoadTable;
        }

        private void LoadTable(object sender, EventArgs e)
        {
            foreach (var pair in table.Highscores.OrderByDescending(key => key.Value))
                highscoreTable.AddRow(new[] { pair.Key, pair.Value.ToString() });
        }
    }
}
