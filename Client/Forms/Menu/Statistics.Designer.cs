﻿namespace Client.Forms.Menu
{
    partial class Statistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.correct = new System.Windows.Forms.Label();
            this.avgTime = new System.Windows.Forms.Label();
            this.wrong = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // correct
            // 
            this.correct.AutoSize = true;
            this.correct.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.correct.Location = new System.Drawing.Point(40, 72);
            this.correct.Name = "correct";
            this.correct.Size = new System.Drawing.Size(234, 32);
            this.correct.TabIndex = 0;
            this.correct.Text = "Correct answers: ";
            // 
            // avgTime
            // 
            this.avgTime.AutoSize = true;
            this.avgTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.avgTime.Location = new System.Drawing.Point(40, 224);
            this.avgTime.Name = "avgTime";
            this.avgTime.Size = new System.Drawing.Size(295, 32);
            this.avgTime.TabIndex = 1;
            this.avgTime.Text = "Average answer time: ";
            // 
            // wrong
            // 
            this.wrong.AutoSize = true;
            this.wrong.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.wrong.Location = new System.Drawing.Point(40, 144);
            this.wrong.Name = "wrong";
            this.wrong.Size = new System.Drawing.Size(225, 32);
            this.wrong.TabIndex = 2;
            this.wrong.Text = "Wrong answers: ";
            // 
            // Statistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 397);
            this.Controls.Add(this.wrong);
            this.Controls.Add(this.avgTime);
            this.Controls.Add(this.correct);
            this.Name = "Statistics";
            this.Text = "Statistics";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label correct;
        private System.Windows.Forms.Label avgTime;
        private System.Windows.Forms.Label wrong;
    }
}