﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Client.Forms.Menu
{
    public partial class Menu : Form
    {
        ClientCommunicator communicator;

        public Menu()
        {
            InitializeComponent();

            Load += Menu_Load;
            FormClosing += Signout;
        }

        private void Menu_Load(object sender, EventArgs e)
        {
            try
            {
                communicator = new ClientCommunicator();
            }
            catch (System.Net.Sockets.SocketException)
            {
                MessageBox.Show("Could not Connect to Server");
                Close();
                return;
            }

            Form login = new Login_Forms.Login(communicator);
            Hide();
            Username = "";
            login.ShowDialog(this);
            if (Username == "")
                Close();
            else
                Show();
        }

        private void Signout(object sender, FormClosingEventArgs e)
        {
            if (communicator == null || !communicator.Connected)
                return;
            communicator.SendMessage(ClientCommunicator.ProtocolCodes.SIGNOUT_REQUEST_CODE);
        }

        public string Username
        {
            get { return string.Concat(user.Text.Skip("Welcome, ".Length)); }
            set { user.Text = "Welcome, " + value; }
        }

        private void Create_room_Click(object sender, EventArgs e)
        {
            Rooms.RoomConfig conf = new Rooms.RoomConfig();
            MessagePacks.Request.CreateRoom room = null;
            conf.FormClosing += (object cSender, FormClosingEventArgs cE) =>
            {
                try
                {
                    room = new MessagePacks.Request.CreateRoom
                    {
                        Name = conf.RoomName,
                        Max_users = conf.MaxUsers,
                        Questions_count = conf.QuestionCount,
                        Time = conf.QuestionTime,
                    };
                }
                catch (TypeException)
                {
                    MessageBox.Show("Invalid parameters.");
                }
                catch (Exception)
                {
                    room = null;
                }
            };
            Hide();
            conf.ShowDialog();
            if (room == null)
            {
                Show();
                return;
            }

            communicator.SendMessage(ClientCommunicator.ProtocolCodes.CREATE_REQUEST_CODE, room);
            var res = communicator.GetMessage<MessagePacks.Response.CreateRoom>(ClientCommunicator.ProtocolCodes.CREATE_REQUEST_CODE);

            if (res.Status == ClientCommunicator.ProtocolCodes.REQUEST_UNSUCCESSFUL)
            {
                Show();
                MessageBox.Show($"Couldn't Create Room: {((MessagePacks.Response.ErrorResponse)res).Message}");
                return;
            }
            MessagePacks.Response.CreateRoom resType = (MessagePacks.Response.CreateRoom)res;
            Form roomForm = new Rooms.AdminRoom(communicator, resType.Id, room.Name, room.Max_users, room.Questions_count, room.Time);
            Hide();

            try
            {
                roomForm.ShowDialog();
            }
            catch (Exception exception)
            {
                HandleException(exception);
            }
            Show();
        }
        private void Join_room_Click(object sender, EventArgs e)
        {
            Rooms.RoomList chooseRoom = new Rooms.RoomList(communicator);
            Hide();
            Form room = null;
            chooseRoom.FormClosing += (object cSender, FormClosingEventArgs cE) => { room = chooseRoom.ChosenRoom; };

            while (chooseRoom.ShowDialog() == DialogResult.Retry) ;     // room is being refreshed
            try
            {
                if (room != null)
                    room.ShowDialog();
            }
            catch (Exception exception)
            {
                HandleException(exception);
            }
            Show();
        }
        private void HandleException(Exception e)
        {
            MessageBox.Show(e.Message);
            Close();
            return;
        }

        private void StatisticsClick(object sender, EventArgs e)
        {
            communicator.SendMessage(ClientCommunicator.ProtocolCodes.GET_PLAYER_STATISTICS_REQUEST_CODE);
            var res = communicator.GetMessage<MessagePacks.Response.Statistics>(ClientCommunicator.ProtocolCodes.GET_PLAYER_STATISTICS_REQUEST_CODE);
            if (res.Status != ClientCommunicator.ProtocolCodes.REQUEST_SUCCESSFUL)
            {
                MessageBox.Show($"Couldn't get statistics: {((MessagePacks.Response.ErrorResponse)res).Message}");
                Close();
                return;
            }
            Form f = new Statistics((MessagePacks.Response.Statistics)res);
            Hide();
            f.ShowDialog();
            Show();
        }
        private void HighscoresClick(object sender, EventArgs e)
        {
            communicator.SendMessage(ClientCommunicator.ProtocolCodes.GET_HIGHSCORES_REQUEST_CODE);
            var res = communicator.GetMessage<MessagePacks.Response.HighscoreTable>(ClientCommunicator.ProtocolCodes.GET_HIGHSCORES_REQUEST_CODE);
            if (res.Status != ClientCommunicator.ProtocolCodes.REQUEST_SUCCESSFUL)
            {
                MessageBox.Show($"Couldn't get statistics: {((MessagePacks.Response.ErrorResponse)res).Message}");
                Close();
                return;
            }
            Form f = new Highscores((MessagePacks.Response.HighscoreTable)res);
            Hide();
            f.ShowDialog();
            Show();
        }
    }
}
