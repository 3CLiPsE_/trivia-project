﻿using System;
using System.Windows.Forms;

namespace Client.Forms.Menu
{
    public partial class Statistics : Form
    {
        readonly MessagePacks.Response.Statistics stats;

        public Statistics(MessagePacks.Response.Statistics stats)
        {
            InitializeComponent();
            this.stats = stats;
            Load += LoadStats;
        }

        void LoadStats(object sender, EventArgs e)
        {
            correct.Text += stats.Correct;
            correct.Location = correct.GetWidthCenter();
            wrong.Text += stats.Wrong;
            wrong.Location = wrong.GetWidthCenter();
            avgTime.Text += String.Format("{0:0.00}", stats.Avg_time);     // show 2 decimal places
            avgTime.Location = avgTime.GetWidthCenter();
        }
    }
}
