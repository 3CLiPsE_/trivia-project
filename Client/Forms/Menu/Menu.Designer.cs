﻿namespace Client.Forms.Menu
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.user = new System.Windows.Forms.Label();
            this.create_room = new System.Windows.Forms.Button();
            this.join_room = new System.Windows.Forms.Button();
            this.statisticsButton = new System.Windows.Forms.Button();
            this.highscoresButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // user
            // 
            this.user.AutoSize = true;
            this.user.Location = new System.Drawing.Point(8, 8);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(74, 17);
            this.user.TabIndex = 0;
            this.user.Text = "Welcome, ";
            // 
            // create_room
            // 
            this.create_room.Location = new System.Drawing.Point(352, 64);
            this.create_room.Name = "create_room";
            this.create_room.Size = new System.Drawing.Size(176, 72);
            this.create_room.TabIndex = 1;
            this.create_room.Text = "Create Room";
            this.create_room.UseVisualStyleBackColor = true;
            this.create_room.Click += new System.EventHandler(this.Create_room_Click);
            // 
            // join_room
            // 
            this.join_room.Location = new System.Drawing.Point(352, 160);
            this.join_room.Name = "join_room";
            this.join_room.Size = new System.Drawing.Size(176, 72);
            this.join_room.TabIndex = 2;
            this.join_room.Text = "Join Room";
            this.join_room.UseVisualStyleBackColor = true;
            this.join_room.Click += new System.EventHandler(this.Join_room_Click);
            // 
            // statisticsButton
            // 
            this.statisticsButton.Location = new System.Drawing.Point(352, 258);
            this.statisticsButton.Name = "statisticsButton";
            this.statisticsButton.Size = new System.Drawing.Size(176, 72);
            this.statisticsButton.TabIndex = 3;
            this.statisticsButton.Text = "Statistics";
            this.statisticsButton.UseVisualStyleBackColor = true;
            this.statisticsButton.Click += new System.EventHandler(this.StatisticsClick);
            // 
            // highscoresButton
            // 
            this.highscoresButton.Location = new System.Drawing.Point(352, 361);
            this.highscoresButton.Name = "highscoresButton";
            this.highscoresButton.Size = new System.Drawing.Size(176, 72);
            this.highscoresButton.TabIndex = 4;
            this.highscoresButton.Text = "Highscores";
            this.highscoresButton.UseVisualStyleBackColor = true;
            this.highscoresButton.Click += new System.EventHandler(this.HighscoresClick);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 512);
            this.Controls.Add(this.highscoresButton);
            this.Controls.Add(this.statisticsButton);
            this.Controls.Add(this.join_room);
            this.Controls.Add(this.create_room);
            this.Controls.Add(this.user);
            this.Name = "Menu";
            this.Text = "Menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label user;
        private System.Windows.Forms.Button create_room;
        private System.Windows.Forms.Button join_room;
        private System.Windows.Forms.Button statisticsButton;
        private System.Windows.Forms.Button highscoresButton;
    }
}