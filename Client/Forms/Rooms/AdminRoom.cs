﻿using System;
using System.Windows.Forms;

namespace Client.Rooms
{
    public partial class AdminRoom : Room
    {
        public AdminRoom(ClientCommunicator communicator, uint id, string name, uint maxPlayers, uint questions, uint timeForQuestion) :
            base(communicator, id, name, maxPlayers, questions, timeForQuestion)
        {
            InitializeComponent();
            start.Click += StartGame;
            leavingMessageCode = ClientCommunicator.ProtocolCodes.CLOSE_ROOM_REQUEST_CODE; //switch the leave code from leave room to close room
        }

        protected async void StartGame(object sender, EventArgs e)
        {
            messagesHandlingCancellation.Cancel();
            await handleMessagesTask; //wait for the Task to close
            // send to server
            communicator.SendMessage(ClientCommunicator.ProtocolCodes.START_GAME_REQUEST_CODE);
            var res = communicator.GetMessage<MessagePacks.Response.NormalResponse>(ClientCommunicator.ProtocolCodes.START_GAME_REQUEST_CODE);

            if (res.Status == ClientCommunicator.ProtocolCodes.REQUEST_SUCCESSFUL)
                base.StartGame();
            else
                MessageBox.Show($"Couldn't start game: {((MessagePacks.Response.ErrorResponse)res).Message}");
            ResetHandlerTask();
        }
    }
}
