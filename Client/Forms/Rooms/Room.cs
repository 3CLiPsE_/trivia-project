﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;
using MessagePack;

namespace Client.Rooms
{
    public partial class Room : Form
    {
        protected uint id;
        protected string name;
        protected uint questions;
        protected uint timeForQuestion;
        protected uint maxPlayers;
        protected ClientCommunicator communicator;
        protected Task handleMessagesTask;
        protected CancellationTokenSource messagesHandlingCancellation;
        protected byte leavingMessageCode;

        public Room(ClientCommunicator communicator, uint id, string name, uint maxPlayers, uint questions, uint timeForQuestion)
        {
            InitializeComponent();

            this.id = id;
            this.communicator = communicator;
            this.name = name;
            this.maxPlayers = maxPlayers;
            this.questions = questions;
            this.timeForQuestion = timeForQuestion;
            leavingMessageCode = ClientCommunicator.ProtocolCodes.LEAVE_ROOM_REQUEST_CODE;
            FormClosing += ExitRoom;
            Load += LoadRoom;
        }

        protected async Task HandleMessagesAsync(CancellationToken token)
        {
            while (true)
            {
                Response toHandle = await Task.Run(() => communicator.GetMessage(token));

                if (toHandle == null)
                    return; //the task was cancelled

                switch (toHandle.Code)
                {
                    case ClientCommunicator.ProtocolCodes.CLOSE_ROOM_REQUEST_CODE:
                        FormClosing -= ExitRoom;    // no need to leave room because the room has already been closed.
                        Close();
                        return;
                    case ClientCommunicator.ProtocolCodes.GET_ROOM_STATE_REQUEST_CODE:
                        RefreshRoom(MessagePackSerializer.Deserialize<MessagePacks.Response.RoomState>(toHandle.Message));
                        break;
                    case ClientCommunicator.ProtocolCodes.START_GAME_REQUEST_CODE:
                        StartGame();
                        break;
                }
            }
        }
        protected void RefreshRoom(MessagePacks.Response.RoomState state)
        {
            if (state.Status != ClientCommunicator.ProtocolCodes.REQUEST_SUCCESSFUL)
                return;
            players.Text = string.Join("\r\n", state.Players);
        }
        protected virtual void StartGame()
        {
            Form gameForm = new Forms.Game.Game(communicator, timeForQuestion, questions);
            Hide();
            gameForm.ShowDialog();
            FormClosing -= ExitRoom;
            Close();
        }
        protected void ResetHandlerTask()
        {
            messagesHandlingCancellation = new CancellationTokenSource();
            handleMessagesTask = HandleMessagesAsync(messagesHandlingCancellation.Token);
        }

        protected async void ExitRoom(object sender, FormClosingEventArgs e)
        {
            messagesHandlingCancellation.Cancel();
            await handleMessagesTask;

            if (!communicator.Connected)
            {
                MessageBox.Show("Couldn't connect to server.");
                Environment.Exit(1); //terminate the program
            }
            communicator.SendMessage(leavingMessageCode);
            var res = communicator.GetMessage<MessagePacks.Response.NormalResponse>(leavingMessageCode);

            if (res.Status != ClientCommunicator.ProtocolCodes.REQUEST_SUCCESSFUL)
                throw new Exception($"Problem While leaving room: {((MessagePacks.Response.ErrorResponse)res).Message}");
        }
        protected void LoadRoom(object sender, EventArgs e)
        {
            communicator.SendMessage(ClientCommunicator.ProtocolCodes.GET_ROOM_STATE_REQUEST_CODE);
            RefreshRoom((MessagePacks.Response.RoomState)communicator.GetMessage<MessagePacks.Response.RoomState>(ClientCommunicator.ProtocolCodes.GET_ROOM_STATE_REQUEST_CODE));

            ResetHandlerTask();
        }
    }
}
