﻿using System;
using System.Windows.Forms;

namespace Client.Rooms
{
    public partial class RoomConfig : Form
    {
        public RoomConfig()
        {
            InitializeComponent();

            maxUsers.KeyPress += CheckInput;
            maxUsers.TextChanged += CheckPasteInput;
            questionCount.KeyPress += CheckInput;
            questionCount.TextChanged += CheckPasteInput;
            questionTime.KeyPress += CheckInput;
            questionTime.TextChanged += CheckPasteInput;
        }

        private void CheckInput(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsControl(e.KeyChar) || char.IsDigit(e.KeyChar)))    // if invalid key
                e.Handled = true;   // ignore
        }
        private void CheckPasteInput(object sender, EventArgs e)
        {
            TextBox toCheck = sender as TextBox;
            if (toCheck.Text != "")
            {
                if (!uint.TryParse(toCheck.Text, out _))
                {
                    toCheck.Text = "";
                    MessageBox.Show("Invalid integer.");
                }
            }
        }
        public string RoomName { get { return name.Text; } }
        public uint MaxUsers { get { return uint.Parse(maxUsers.Text); } }
        public uint QuestionCount { get { return uint.Parse(questionCount.Text); } }
        public uint QuestionTime { get { return uint.Parse(questionTime.Text); } }

        private void Confirm_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
