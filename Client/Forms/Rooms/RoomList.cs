﻿using System;
using System.Windows.Forms;
using MessagePack;

namespace Client.Rooms
{
    public partial class RoomList : Form
    {
        readonly ClientCommunicator communicator;

        public RoomList(ClientCommunicator communicator)
        {
            InitializeComponent();
            this.communicator = communicator;

            Load += RoomList_Load;
        }

        private void RoomList_Load(object sender, EventArgs e)
        {
            const int BUTTON_DISTANCE = 72;
            const int FIRST_Y_POS = 16;
            System.Drawing.Size buttonSize = new System.Drawing.Size(320, 64);
            int X_POS = (Size.Width / 2) - (buttonSize.Width / 2) + (refresh.Size.Width / 2);

            var rooms = GetRooms();
            if (rooms.Length == 0)
            {
                MessageBox.Show("No rooms available.");
                Close();
                return;
            }
            roomButtons = new RoomButton[rooms.Length];

            for (int i = 0; i < rooms.Length; i++)
            {
                roomButtons[i] = new RoomButton
                {
                    Location = new System.Drawing.Point(X_POS, FIRST_Y_POS + BUTTON_DISTANCE * i),
                    Size = buttonSize,
                    Room = rooms[i],
                };
                roomButtons[i].Click += ChooseRoom;
                roomButtons[i].ConfigureButton();
                Controls.Add(roomButtons[i]);
            }
        }

        public Form ChosenRoom { get; set; }

        private void ChooseRoom(object sender, EventArgs e)
        {
            if (sender is RoomButton button)
            {
                var data = new MessagePacks.Request.JoinRoom
                {
                    Room_id = button.Room.Id
                };

                communicator.SendMessage(ClientCommunicator.ProtocolCodes.JOIN_REQUEST_CODE, data);
                var res = communicator.GetMessage<MessagePacks.Response.NormalResponse>(ClientCommunicator.ProtocolCodes.JOIN_REQUEST_CODE);

                if (res.Status == ClientCommunicator.ProtocolCodes.REQUEST_SUCCESSFUL)
                {
                    ChosenRoom = new Rooms.Room(communicator, button.Room.Id, button.Room.Name, button.Room.Max_players, button.Room.Question_num, button.Room.Question_time);
                    Close();
                }
                else
                {
                    MessageBox.Show($"Couldn't Join Selected Room: {((MessagePacks.Response.ErrorResponse)res).Message}");
                }
            }
            else
            {
                throw new Exception("Event is not from a button");
            }
        }
        private MessagePacks.Response.Room[] GetRooms()
        {
            communicator.SendMessage(ClientCommunicator.ProtocolCodes.GET_ROOMS_REQUEST_CODE);
            var res = communicator.GetMessage<MessagePacks.Response.RoomList>(ClientCommunicator.ProtocolCodes.GET_ROOMS_REQUEST_CODE);
            if (res.Status != ClientCommunicator.ProtocolCodes.REQUEST_SUCCESSFUL)
                throw new Exception($"Problem occured while trying to retrieve rooms: {((MessagePacks.Response.ErrorResponse)res).Message}");

            return ((MessagePacks.Response.RoomList)res).Rooms;
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Retry;
            Close();
        }
    }
    public class RoomButton : Button
    {
        public MessagePacks.Response.Room Room { get; set; }
        public void ConfigureButton()
        {
            TabIndex = 0;
            UseVisualStyleBackColor = true;
            Text = $"{Room.Id} - {Room.Name} (Max {Room.Max_players} players)\r\n" +
                $"{Room.Question_num} questions, {Room.Question_time} seconds each.\r\n" +
                $"Room {(Room.Active ? "is" : "isn't")} active.";
        }
    }
}
