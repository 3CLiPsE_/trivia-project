﻿namespace Client.Rooms
{
    partial class RoomConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.name = new System.Windows.Forms.WatermarkTextBox();
            this.questionTime = new System.Windows.Forms.WatermarkTextBox();
            this.questionCount = new System.Windows.Forms.WatermarkTextBox();
            this.maxUsers = new System.Windows.Forms.WatermarkTextBox();
            this.confirm = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // name
            // 
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.name.Location = new System.Drawing.Point(48, 40);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(248, 22);
            this.name.TabIndex = 0;
            this.name.WaterMarkColor = System.Drawing.Color.Gray;
            this.name.WaterMarkText = "Enter Name...";
            // 
            // questionTime
            // 
            this.questionTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.questionTime.Location = new System.Drawing.Point(48, 136);
            this.questionTime.Name = "questionTime";
            this.questionTime.Size = new System.Drawing.Size(248, 22);
            this.questionTime.TabIndex = 3;
            this.questionTime.WaterMarkColor = System.Drawing.Color.Gray;
            this.questionTime.WaterMarkText = "Enter Time for Question...";
            // 
            // questionCount
            // 
            this.questionCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.questionCount.Location = new System.Drawing.Point(48, 104);
            this.questionCount.Name = "questionCount";
            this.questionCount.Size = new System.Drawing.Size(248, 22);
            this.questionCount.TabIndex = 2;
            this.questionCount.WaterMarkColor = System.Drawing.Color.Gray;
            this.questionCount.WaterMarkText = "Enter Number of Questions...";
            // 
            // maxUsers
            // 
            this.maxUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F);
            this.maxUsers.Location = new System.Drawing.Point(48, 72);
            this.maxUsers.Name = "maxUsers";
            this.maxUsers.Size = new System.Drawing.Size(248, 22);
            this.maxUsers.TabIndex = 1;
            this.maxUsers.WaterMarkColor = System.Drawing.Color.Gray;
            this.maxUsers.WaterMarkText = "Enter Max Users...";
            // 
            // confirm
            // 
            this.confirm.Location = new System.Drawing.Point(48, 176);
            this.confirm.Name = "confirm";
            this.confirm.Size = new System.Drawing.Size(120, 32);
            this.confirm.TabIndex = 4;
            this.confirm.Text = "OK";
            this.confirm.UseVisualStyleBackColor = true;
            this.confirm.Click += new System.EventHandler(this.Confirm_Click);
            // 
            // back
            // 
            this.back.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.back.Location = new System.Drawing.Point(176, 176);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(120, 32);
            this.back.TabIndex = 5;
            this.back.Text = "Back";
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.Back_Click);
            // 
            // RoomConfig
            // 
            this.AcceptButton = this.confirm;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.back;
            this.ClientSize = new System.Drawing.Size(348, 231);
            this.Controls.Add(this.back);
            this.Controls.Add(this.confirm);
            this.Controls.Add(this.maxUsers);
            this.Controls.Add(this.questionCount);
            this.Controls.Add(this.questionTime);
            this.Controls.Add(this.name);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RoomConfig";
            this.Text = "1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WatermarkTextBox name;
        private System.Windows.Forms.WatermarkTextBox questionTime;
        private System.Windows.Forms.WatermarkTextBox questionCount;
        private System.Windows.Forms.WatermarkTextBox maxUsers;
        private System.Windows.Forms.Button confirm;
        private System.Windows.Forms.Button back;
    }
}