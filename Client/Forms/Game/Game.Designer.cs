﻿namespace Client.Forms.Game
{
    partial class Game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gameTimer = new System.Windows.Forms.Timer(this.components);
            this.timeLeft = new System.Windows.Forms.Label();
            this.question = new System.Windows.Forms.Label();
            this.questionCount = new System.Windows.Forms.Label();
            this.ans0 = new System.Windows.Forms.Button();
            this.ans1 = new System.Windows.Forms.Button();
            this.ans2 = new System.Windows.Forms.Button();
            this.ans3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // gameTimer
            // 
            this.gameTimer.Interval = 1000;
            // 
            // timeLeft
            // 
            this.timeLeft.AutoSize = true;
            this.timeLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.timeLeft.Location = new System.Drawing.Point(384, 352);
            this.timeLeft.Name = "timeLeft";
            this.timeLeft.Size = new System.Drawing.Size(42, 46);
            this.timeLeft.TabIndex = 0;
            this.timeLeft.Text = "5";
            // 
            // question
            // 
            this.question.AutoSize = true;
            this.question.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.question.Location = new System.Drawing.Point(200, 16);
            this.question.Name = "question";
            this.question.Size = new System.Drawing.Size(407, 44);
            this.question.TabIndex = 1;
            this.question.Text = "Quesiton number one?";
            // 
            // questionCount
            // 
            this.questionCount.AutoSize = true;
            this.questionCount.Location = new System.Drawing.Point(8, 8);
            this.questionCount.Name = "questionCount";
            this.questionCount.Size = new System.Drawing.Size(28, 17);
            this.questionCount.TabIndex = 6;
            this.questionCount.Text = "0/?";
            // 
            // ans0
            // 
            this.ans0.Location = new System.Drawing.Point(80, 88);
            this.ans0.Name = "ans0";
            this.ans0.Size = new System.Drawing.Size(264, 96);
            this.ans0.TabIndex = 7;
            this.ans0.Text = "button1";
            this.ans0.UseVisualStyleBackColor = true;
            // 
            // ans1
            // 
            this.ans1.Location = new System.Drawing.Point(456, 88);
            this.ans1.Name = "ans1";
            this.ans1.Size = new System.Drawing.Size(264, 96);
            this.ans1.TabIndex = 8;
            this.ans1.Text = "button2";
            this.ans1.UseVisualStyleBackColor = true;
            // 
            // ans2
            // 
            this.ans2.Location = new System.Drawing.Point(80, 240);
            this.ans2.Name = "ans2";
            this.ans2.Size = new System.Drawing.Size(264, 96);
            this.ans2.TabIndex = 9;
            this.ans2.Text = "button3";
            this.ans2.UseVisualStyleBackColor = true;
            // 
            // ans3
            // 
            this.ans3.Location = new System.Drawing.Point(456, 240);
            this.ans3.Name = "ans3";
            this.ans3.Size = new System.Drawing.Size(264, 96);
            this.ans3.TabIndex = 10;
            this.ans3.Text = "button4";
            this.ans3.UseVisualStyleBackColor = true;
            // 
            // Game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ans3);
            this.Controls.Add(this.ans2);
            this.Controls.Add(this.ans1);
            this.Controls.Add(this.ans0);
            this.Controls.Add(this.questionCount);
            this.Controls.Add(this.question);
            this.Controls.Add(this.timeLeft);
            this.Name = "Game";
            this.Text = "Game";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer gameTimer;
        private System.Windows.Forms.Label timeLeft;
        private System.Windows.Forms.Label question;
        private System.Windows.Forms.Label questionCount;
        private System.Windows.Forms.Button ans0;
        private System.Windows.Forms.Button ans1;
        private System.Windows.Forms.Button ans2;
        private System.Windows.Forms.Button ans3;
        private System.Windows.Forms.Button[] answers;
    }
}