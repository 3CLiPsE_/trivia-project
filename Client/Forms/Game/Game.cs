﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client.Forms.Game
{
    public partial class Game : Form
    {
        readonly ClientCommunicator communicator;
        readonly uint timeForQuestion;
        readonly uint numberOfQuestions;
        uint questionCounter;
        public Game(ClientCommunicator communicator, uint timeForQuestion, uint numberOfQuestions)
        {
            questionCounter = 0;
            this.communicator = communicator;
            this.timeForQuestion = timeForQuestion;
            this.numberOfQuestions = numberOfQuestions;
            InitializeComponent();
            answers = new[] { ans0, ans1, ans2, ans3 };
            timeLeft.TextChanged += CentralizeLabel;
            question.TextChanged += CentralizeLabel;
            gameTimer.Tick += AdvanceTimerLabel;
            FormClosing += FinishGame;
            Load += FormLoading;
            for (int i = 0; i < answers.Length; i++)
                answers[i].Click += SubmitAnswer;
        }

        private void FinishGame(object sender, FormClosingEventArgs e)
        {
            gameTimer.Stop();
            Form results = new GameStats(communicator);
            Hide();
            results.ShowDialog();
        }
        private void CentralizeLabel(object sender, EventArgs e)
        {
            Label label = sender as Label;
            label.Location = label.GetWidthCenter();
        }
        private async void AdvanceTimerLabel(object sender, EventArgs e)
        {
            uint timeLeft = uint.Parse(this.timeLeft.Text) - 1;
            if (timeLeft == 0)
            {
                var code = communicator.GetMessage().Code;
                if (code == ClientCommunicator.ProtocolCodes.TIME_OUT_RESPONSE_CODE)
                    await AdvanceGame();
                else
                    throw new TypeException("Unexpected code.")
                    {
                        Expected = ClientCommunicator.ProtocolCodes.TIME_OUT_RESPONSE_CODE,
                        Got = code,
                    };
            }
            else
                this.timeLeft.Text = timeLeft.ToString();
        }
        private void ResetTimer()
        {
            gameTimer.Stop();
            timeLeft.Text = timeForQuestion.ToString();
            gameTimer.Start();
        }
        private void GetAnswer()
        {
            var answer = (MessagePacks.Response.GetAnswer)communicator.GetMessage<MessagePacks.Response.GetAnswer>(ClientCommunicator.ProtocolCodes.SUBMIT_ANSWER_REQUEST_CODE);
            answers[answer.Correct_answer_index].BackColor = System.Drawing.Color.LimeGreen;
        }
        private async Task AdvanceGame()
        {
            communicator.SendMessage(ClientCommunicator.ProtocolCodes.GET_QUESTION_REQUEST_CODE);
            var res = await Task.Run(() => communicator.GetMessage<MessagePacks.Response.GetQuestion>(ClientCommunicator.ProtocolCodes.GET_QUESTION_REQUEST_CODE));

            if (res.Status != ClientCommunicator.ProtocolCodes.REQUEST_SUCCESSFUL)
            {
                Close();
                return;
            }
            var nextQuestion = res as MessagePacks.Response.GetQuestion;
            questionCounter++;
            question.Text = nextQuestion.Question;
            questionCount.Text = $"{questionCounter}/{numberOfQuestions}";
            for (int i = 0; i < answers.Length; i++)
            {
                answers[i].Text = nextQuestion.Answers[i];
                answers[i].BackColor = default;
                answers[i].UseVisualStyleBackColor = true;
                answers[i].Enabled = true;
            }
            ResetTimer();
        }
        private async void SubmitAnswer(object sender, EventArgs e)
        {
            Button b = sender as Button;
            try
            {
                communicator.SendMessage(ClientCommunicator.ProtocolCodes.SUBMIT_ANSWER_REQUEST_CODE,
                new MessagePacks.Request.SubmitAnswer { Answer_Id = Array.IndexOf(answers, b) });

                b.BackColor = System.Drawing.Color.OrangeRed;
                GetAnswer();

                foreach (Button button in answers)
                    button.Enabled = false;
            }
            catch (TypeException ex)
            {
                if (ex.Got != ClientCommunicator.ProtocolCodes.TIME_OUT_RESPONSE_CODE)
                    throw ex;
            }
            Thread.Sleep(new TimeSpan(0, 0, 1));
            await AdvanceGame();
        }
        private async void FormLoading(object sender, EventArgs e)
        {
            await AdvanceGame();
        }
    }
}
