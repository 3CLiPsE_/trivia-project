﻿using System;
using System.Windows.Forms;

namespace Client.Forms.Game
{
    public partial class GameStats : Form
    {
        readonly ClientCommunicator communicator;

        public GameStats(ClientCommunicator communicator)
        {
            InitializeComponent();
            this.communicator = communicator;
            Load += GameStats_Load;
        }

        private void GameStats_Load(object sender, EventArgs e)
        {
            communicator.SendMessage(ClientCommunicator.ProtocolCodes.GET_GAME_RESULTS_REQUEST_CODE);
            var res = communicator.GetMessage<MessagePacks.Response.GameResults>(ClientCommunicator.ProtocolCodes.GET_GAME_RESULTS_REQUEST_CODE);
            if (res.Status != ClientCommunicator.ProtocolCodes.REQUEST_SUCCESSFUL)
                throw new Exception($"Can't get game results: {((MessagePacks.Response.ErrorResponse)res).Message}");
            foreach (MessagePacks.Response.GameResult result in ((MessagePacks.Response.GameResults)res).Results)
                results.AddRow(new[] { result.Username, $"{result.Correct}/{result.Wrong}", result.Avg_time.ToString() });
        }
    }
}
