﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;

namespace Client
{
    public static class TableExtension
    {
        // Taken from https://stackoverflow.com/a/40884366/9165088
        public static void AddRow(this TableLayoutPanel table, IList<string> values)
        {
            if (table.ColumnCount != values.Count)
                throw new Exception("Elements number doesn't match!");
            //get a reference to the previous existent row
            RowStyle temp = table.RowStyles[table.RowCount - 1];
            //increase panel rows count by one
            table.RowCount++;
            //add a new RowStyle as a copy of the previous one
            table.RowStyles.Add(new RowStyle(temp.SizeType, temp.Height));
            //add the control
            for (int i = 0; i < values.Count; i++)
                table.Controls.Add(new Label() { Text = values[i] }, i, table.RowCount - 1);
        }
    }
    public static class StreamExtention
    {
        public static byte[] Read(this NetworkStream stream, int bytesToRead, CancellationToken token)
        {
            while (!stream.DataAvailable)
            {
                token.ThrowIfCancellationRequested();
                Thread.Sleep(100);
            }

            byte[] buffer = new byte[bytesToRead];
            stream.Read(buffer, 0, bytesToRead);
            return buffer;
        }

        public static int ReadByte(this NetworkStream stream, CancellationToken token)
        {
            while (!stream.DataAvailable)
            {
                token.ThrowIfCancellationRequested();
                Thread.Sleep(100);
            }

            return stream.ReadByte();
        }
    }
    public static class LabelExtension
    {
        public static Point GetWidthCenter(this Label label)
        {
            return new Point(
                label.FindForm().Size.Width / 2 - label.Size.Width / 2,
                label.Location.Y);
        }
    }
}

namespace System.Windows.Forms
{
    // Taken from https://www.codeproject.com/Articles/27849/WaterMark-TextBox-For-Desktop-Applications-Using-C
    public class WatermarkTextBox : TextBox
    {
        private Font oldFont = null;
        private Boolean waterMarkTextEnabled = false;

        #region Attributes 
        private Color _waterMarkColor = Color.Gray;
        public Color WaterMarkColor
        {
            get { return _waterMarkColor; }
            set
            {
                _waterMarkColor = value; Invalidate();/*thanks to Bernhard Elbl
                                                              for Invalidate()*/
            }
        }

        private string _waterMarkText = "Water Mark";
        public string WaterMarkText
        {
            get { return _waterMarkText; }
            set { _waterMarkText = value; Invalidate(); }
        }
        #endregion

        //Default constructor
        public WatermarkTextBox()
        {
            JoinEvents(true);
        }

        //Override OnCreateControl ... thanks to  "lpgray .. codeproject guy"
        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            WaterMark_Toggel(null, null);
        }

        //Override OnPaint
        protected override void OnPaint(PaintEventArgs args)
        {
            // Use the same font that was defined in base class
            System.Drawing.Font drawFont = new System.Drawing.Font(Font.FontFamily,
                Font.Size, Font.Style, Font.Unit);
            //Create new brush with gray color or 
            SolidBrush drawBrush = new SolidBrush(WaterMarkColor);//use Water mark color
            //Draw Text or WaterMark
            args.Graphics.DrawString((waterMarkTextEnabled ? WaterMarkText : Text),
                drawFont, drawBrush, new PointF(0.0F, 0.0F));
            base.OnPaint(args);
        }

        private void JoinEvents(Boolean join)
        {
            if (join)
            {
                this.TextChanged += new System.EventHandler(this.WaterMark_Toggel);
                this.LostFocus += new System.EventHandler(this.WaterMark_Toggel);
                this.FontChanged += new System.EventHandler(this.WaterMark_FontChanged);
                //No one of the above events will start immeddiatlly 
                //TextBox control still in constructing, so,
                //Font object (for example) couldn't be catched from within
                //WaterMark_Toggle
                //So, call WaterMark_Toggel through OnCreateControl after TextBox
                //is totally created
                //No doupt, it will be only one time call

                //Old solution uses Timer.Tick event to check Create property
            }
        }

        private void WaterMark_Toggel(object sender, EventArgs args)
        {
            if (this.Text.Length <= 0)
                EnableWaterMark();
            else
                DisbaleWaterMark();
        }

        private void EnableWaterMark()
        {
            //Save current font until returning the UserPaint style to false (NOTE:
            //It is a try and error advice)
            oldFont = new System.Drawing.Font(Font.FontFamily, Font.Size, Font.Style,
               Font.Unit);
            //Enable OnPaint event handler
            this.SetStyle(ControlStyles.UserPaint, true);
            this.waterMarkTextEnabled = true;
            //Triger OnPaint immediatly
            Refresh();
        }

        private void DisbaleWaterMark()
        {
            //Disbale OnPaint event handler
            this.waterMarkTextEnabled = false;
            this.SetStyle(ControlStyles.UserPaint, false);
            //Return back oldFont if existed
            if (oldFont != null)
                this.Font = new System.Drawing.Font(oldFont.FontFamily, oldFont.Size,
                    oldFont.Style, oldFont.Unit);
        }

        private void WaterMark_FontChanged(object sender, EventArgs args)
        {
            if (waterMarkTextEnabled)
            {
                oldFont = new System.Drawing.Font(Font.FontFamily, Font.Size, Font.Style,
                    Font.Unit);
                Refresh();
            }
        }
    }
}
