#pragma once

#include "LoggedUser.h"
#include "Room.h"
#include <vector>
#include <string>


struct CloseRoomResponse
{
	unsigned int _status;

	CloseRoomResponse(unsigned int status);
};

struct StartRoomResponse
{
	unsigned int _status;

	StartRoomResponse(unsigned int status);
};

struct GetRoomStateResponse
{
	unsigned int _status;
	bool _hasGameBegun;
	std::vector<std::string> _players;
	unsigned int _questionCount;
	unsigned int _answerTimeout;

	GetRoomStateResponse(unsigned int status, const Room* room);
};

struct LeaveRoomResponse
{
	unsigned int _status;

	LeaveRoomResponse(unsigned int status);
};
