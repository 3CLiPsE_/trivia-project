#pragma once

#include <string>


class LoggedUser
{
public:
	LoggedUser(const std::string& username, unsigned int id);
	LoggedUser(const LoggedUser& other);

	std::string getUsername() const;
	unsigned int getId() const;

	bool operator==(const LoggedUser& other) const;
	bool operator!=(const LoggedUser& other) const;

private:
	std::string _username;
	unsigned int _id;
};

namespace std
{
	template <>
	struct hash<LoggedUser>		// create hash function use map
	{
		size_t operator()(const LoggedUser& user) const noexcept
		{

			std::size_t h1(std::hash<std::string>() (user.getUsername()));
			std::size_t h2(std::hash<int>() (user.getId()));

			return h1 ^ (h2 << 1);	// xor hash of name and id to create new hash of both
		}
	};
}
