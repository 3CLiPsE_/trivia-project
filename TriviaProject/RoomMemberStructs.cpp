#include "RoomMemberStructs.h"


CloseRoomResponse::CloseRoomResponse(unsigned int status) : _status(status)
{

}

StartRoomResponse::StartRoomResponse(unsigned int status) : _status(status)
{

}

GetRoomStateResponse::GetRoomStateResponse(unsigned int status, const Room* room) :
	_status(status), _hasGameBegun(room->getRoomData()._isActive), _answerTimeout(room->getRoomData()._timePerQuestion), _questionCount(room->getRoomData()._questionCount)
{

	for (auto player : room->getAllUsers())
		_players.push_back(player.getUsername());
}

LeaveRoomResponse::LeaveRoomResponse(unsigned int status) : _status(status)
{

}
