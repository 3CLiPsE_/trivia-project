#include "IRoomRequestHandler.h"
#include "RoomData.h"
#include "ProtocolCodes.h"
#include "JsonResponsePacketSerializer.h"


IRoomRequestHandler::IRoomRequestHandler(RoomManager& roomManager, Room* room, const LoggedUser& user) :
	_roomManager(roomManager), _user(user), _room(room)
{

}

RequestResult IRoomRequestHandler::getRoomState(const Request& request)
{
	RoomData data = _room->getRoomData();

	return RequestResult(GET_ROOM_STATE_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse(REQUEST_SUCCESSFUL, _room)),
		nullptr);
}
