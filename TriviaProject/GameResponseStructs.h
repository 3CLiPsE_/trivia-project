#pragma once

#include <string>
#include "Question.h"
#include "GameData.h"
#include "LoggedUser.h"
#include <unordered_map> 
#include <vector>


struct GetQuestionResponse
{
	unsigned int _status;
	std::string _question;
	std::vector<std::string> _answers;

	GetQuestionResponse(unsigned int status, const Question& q);
};

struct SubmitAnswerResponse
{
	unsigned int _status;
	unsigned int _correctAnswer;

	SubmitAnswerResponse(unsigned int status, unsigned int correctAnswer);
};

struct PlayerList
{
	std::string _username;
	unsigned int _correctAnswerCount;
	unsigned int _wrongAnswerCount;
	unsigned int _averageAnswerTime;

	PlayerList(const std::string& username, unsigned int correctAnswerCount, unsigned int wrongAnswerCount, unsigned int averageAnswerTime);
};

struct GetGameResultResponse
{
	unsigned int _status;
	std::vector<PlayerList> _results;

	GetGameResultResponse(unsigned int status, const std::unordered_map<LoggedUser, GameData>& results);
};
