#include "Request.h"
#include "ProtocolCodes.h"


Request::Request(int requestId, const CTime& recivalTime, const std::vector<char>& buffer) :
	_requestId(requestId), _recivalTime(recivalTime), _buffer(buffer)
{

}
Request::Request(int requestId, const std::vector<char>& buffer) :
	_requestId(requestId), _recivalTime(CTime::GetCurrentTime()), _buffer(buffer)
{

}

bool Request::isSignout()
{
	return _requestId == SIGNOUT_REQUEST_CODE;
}
