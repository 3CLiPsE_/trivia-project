#pragma once

#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "IRoomRequestHandler.h"


class RoomAdminHandler : public IRoomRequestHandler
{
public:
	RoomAdminHandler(Room* room, const LoggedUser& user, RoomManager& roomManager, RequestHandlerFactory* handlerFactory);
	~RoomAdminHandler();

	virtual bool isRequestRelevant(const Request& request);
	virtual RequestResult handleRequest(const Request& request);

private:
	RequestHandlerFactory* _handlerFactory;

	RequestResult closeRoom(const Request& request);
	RequestResult startGame(const Request& request);
};
