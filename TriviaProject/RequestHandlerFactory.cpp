#include "RequestHandlerFactory.h"
#include "MyException.h"


RequestHandlerFactory::RequestHandlerFactory(IDatabase& database) :
	_loginManager(database), _roomManager(database), _highscoreTable(database), _gameManager(database)
{

}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler(unsigned int id)
{
	return new LoginRequestHandler(_loginManager, this, id);
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(const LoggedUser& user)
{
	return new MenuRequestHandler(_roomManager, _loginManager, this, user, _highscoreTable);
}

RoomMemberHandler* RequestHandlerFactory::createRoomMemberRequestHandler(Room* room, const LoggedUser& user)
{
	return new RoomMemberHandler(room, user, _roomManager, this);
}

RoomAdminHandler* RequestHandlerFactory::createRoomAdminRequestHandler(Room* room, const LoggedUser& user)
{
	return new RoomAdminHandler(room, user, _roomManager, this);
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(Room* room, const LoggedUser& user)
{
	return new GameRequestHandler(_gameManager, _roomManager, _gameManager.CreateGame(room), user, this);
}
