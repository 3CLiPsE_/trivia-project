#include "Helper.h"
#include "ErrorResponse.h"
#include "MyException.h"
#include "JsonResponsePacketSerializer.h"
#include "Communicator.h"
#include "ProtocolCodes.h"

#define BIT_NUMBER_IN_BYTE 8


std::vector<unsigned char> Helper::intToBytes(int number)
{
	// code taken from stackoverflow
	// https://stackoverflow.com/questions/5585532/c-int-to-byte-array
	std::vector<unsigned char> result(sizeof(int));

	for (int i = 0; i < sizeof(int); i++)
		result[sizeof(int) - i - 1] = number >> (i * BIT_NUMBER_IN_BYTE);
	return result;
}

RequestResult Helper::getErrorResult(unsigned int code, const std::string & msg)
{
	return RequestResult(code, JsonResponsePacketSerializer::serializeResponse(ErrorResponse(msg)), nullptr);
}

std::vector<char> Helper::toCharArray(std::vector<uint8_t> src)
{
	auto result = reinterpret_cast<char*>(src.data());
	return std::vector<char>(result, result + src.size());
}

void Helper::addNotificationToQueue(byte code, const std::vector<char> & buffer, const std::vector<LoggedUser> & targets, RequestHandlerFactory * handlerFactory, void* additionalData)
{
	if ((code == GET_ROOM_STATE_REQUEST_CODE) || (code == TIME_OUT_RESPONSE_CODE) || (code == GET_QUESTION_REQUEST_CODE))
	{
		for (auto& user : targets)
			Communicator::addRequestToQueue(RequestResult(code, buffer, nullptr), user.getId());
	}
	else
	{
		switch (code)
		{
		case CLOSE_ROOM_REQUEST_CODE:
		{
			for (auto& user : targets)
				Communicator::addRequestToQueue(RequestResult(code, buffer, handlerFactory->createMenuRequestHandler(user)), user.getId());
			break;
		}
		case START_GAME_REQUEST_CODE:
		{
			Room* room = (Room*)additionalData;

			for (auto& target : targets)
				Communicator::addRequestToQueue(
					RequestResult(code, buffer, handlerFactory->createGameRequestHandler(room, target))
					, target.getId());

			break;
		}
		default:
			throw MyException("Unsupported notification type");
			break;
		}
	}
}
