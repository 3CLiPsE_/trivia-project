#include "HighscoreTable.h"

HighscoreTable::HighscoreTable(IDatabase& database) : 
	_database(database)
{

}

void HighscoreTable::insertScore(const std::string& user, int game_id, int correctAns, int wrongAns, int ansCount, int totalAnsTime)
{
	_database.addStatistic(user, game_id, correctAns, wrongAns, totalAnsTime, ansCount);
}
std::map<std::string, int> HighscoreTable::getHighscores() const
{
	return _database.getHighScores();
}
PlayerStatistics HighscoreTable::getPlayerStatistics(const std::string& user) const
{
	return _database.getPlayerStatistics(user);
}
