#pragma once

#include "RequestResult.h"
#include "LoggedUser.h"
#include "RequestHandlerFactory.h"
#include <vector>


class Helper
{
public:
	static std::vector<char> toCharArray(std::vector<uint8_t> src);
	static std::vector<unsigned char> intToBytes(int number);
	static RequestResult getErrorResult(unsigned int code, const std::string& msg);

	static void addNotificationToQueue(byte code, const std::vector<char>& buffer, const std::vector<LoggedUser>& targets, RequestHandlerFactory* handlerFactory, void* additionalData = nullptr);	
};
