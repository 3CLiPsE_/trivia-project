#pragma once

#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "ProtocolCodes.h"
#include <WinSock2.h>
#include <Windows.h>
#include <map>
#include <queue>
#include <mutex>
#include <shared_mutex>

#define PORT 1337


class Communicator
{
public:
	Communicator(RequestHandlerFactory* handlerFactory);
	~Communicator();

	void bindAndListen();
	void handleRequests();

	static void addRequestToQueue(RequestResult result , int userId);

private:
	static std::queue<std::pair<RequestResult, SOCKET>> _messageQueue;
	static std::map<unsigned int, SOCKET> _idSocketMap;
	static std::map<SOCKET, IRequestHandler*> _clients;
	static std::mutex _messageMutex;
	static std::shared_mutex _clientsMutex;
	static std::shared_mutex _idsMutex;
	static std::condition_variable _messageQueueCond;

	unsigned int _nextClientId;
	RequestHandlerFactory* _handlerFactory;
	SOCKET _serverSock;

	Request getRequest(SOCKET client);
	void startThreadForNewClient(SOCKET client);
	void startThreadForNotifications();
};

#ifdef _DEBUG // vs add this define in debug mode
#include <stdio.h>
// Q: why do we need traces ?
// A: traces are a nice and easy way to detect bugs without even debugging
// or to understand what happened in case we miss the bug in the first time
#define TRACE(msg, ...) printf(msg, __VA_ARGS__);
// for convenient reasons we did the traces in stdout
// at general we would do this in the error stream like that
// #define TRACE(msg, ...) fprintf(stderr, msg "\n", __VA_ARGS__);

#else // we want nothing to be printed in release version
#define TRACE(msg, ...) // do nothing
#endif
