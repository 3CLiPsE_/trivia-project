#pragma once

#include "IDatabase.h"
#include "Communicator.h"
#include "WSAInit.h"


class Server
{
public:
	Server(IDatabase& database);
	~Server();
	void run();

private:
	WSAInit _wsaInit;
	RequestHandlerFactory _handlerFactory;
	Communicator _communicator;
};
