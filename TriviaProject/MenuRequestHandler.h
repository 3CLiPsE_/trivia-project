#pragma once

#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "LoggedUser.h"
#include "HighscoreTable.h"
#include "RequestHandlerFactory.h"


class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(RoomManager& roomManager, LoginManager& loginManager, RequestHandlerFactory* handlerFactory, const LoggedUser& user, const HighscoreTable& highscoreTable);
	~MenuRequestHandler();

	virtual bool isRequestRelevant(const Request& request);
	virtual RequestResult handleRequest(const Request& request);

private:
	LoggedUser _user;
	RoomManager& _roomManager;
	LoginManager& _loginManager;
	HighscoreTable _highscroeTable;
	RequestHandlerFactory* _handlerFactory;

	RequestResult signout(const Request& request);
	RequestResult getRooms(const Request& request);
	RequestResult getPlayersInRoom(const Request& request);
	RequestResult getHighscores(const Request& request);
	RequestResult getPlayerStatistics(const Request& request);
	RequestResult joinRoom(const Request& request);
	RequestResult createRoom(const Request& request);
};
