#pragma once

#include "LoginManager.h"
#include "RoomManager.h"
#include "GameManager.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomMemberHandler.h"
#include "RoomAdminHandler.h"
#include "GameRequestHandler.h"
#include "HighscoreTable.h"


class LoginRequestHandler;
class MenuRequestHandler;
class RoomMemberHandler;
class RoomAdminHandler;
class GameRequestHandler;

class RequestHandlerFactory
{
public:
	RequestHandlerFactory(IDatabase& database);

	LoginRequestHandler* createLoginRequestHandler(unsigned int id);
	MenuRequestHandler* createMenuRequestHandler(const LoggedUser& user);
	RoomMemberHandler* createRoomMemberRequestHandler(Room* room, const LoggedUser& user);
	RoomAdminHandler* createRoomAdminRequestHandler(Room* room, const LoggedUser& user);
	GameRequestHandler* createGameRequestHandler(Room* room, const LoggedUser& user);

private:
	LoginManager _loginManager;
	RoomManager _roomManager;
	HighscoreTable _highscoreTable;
	GameManager _gameManager;

};
