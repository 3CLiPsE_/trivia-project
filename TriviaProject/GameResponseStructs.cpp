#include "GameResponseStructs.h"


GetQuestionResponse::GetQuestionResponse(unsigned int status, const Question& q) :
	_status(status), _question(q.getQuestion())
{
	for (auto& answer : q.getPossibleAnswers())
		_answers.push_back(answer);
}

SubmitAnswerResponse::SubmitAnswerResponse(unsigned int status, unsigned int correctAnswer) :
	_status(status), _correctAnswer(correctAnswer)
{

}

PlayerList::PlayerList(const std::string & username, unsigned int correctAnswerCount, unsigned int wrongAnswerCount, unsigned int averageAnswerTime) :
	_username(username), _correctAnswerCount(correctAnswerCount), _wrongAnswerCount(wrongAnswerCount), _averageAnswerTime(averageAnswerTime)
{

}

GetGameResultResponse::GetGameResultResponse(unsigned int status, const std::unordered_map<LoggedUser, GameData> & results) :
	_status(status)
{
	for (auto& userResultPair : results)
		_results.push_back(PlayerList(userResultPair.first.getUsername(), userResultPair.second._correctAnswerCount,
			userResultPair.second._wrongAnswerCount, userResultPair.second._averageAnswerTime));
}
