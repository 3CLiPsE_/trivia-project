#include "RoomData.h"


RoomData::RoomData(unsigned int id, const std::string& name, unsigned int maxPlayers, unsigned int timePerQuestion, unsigned int questionCount, bool isActive) :
	_id(id), _name(name), _maxPlayers(maxPlayers), _timePerQuestion(timePerQuestion), _questionCount(questionCount), _isActive(isActive)
{

}

bool RoomData::operator==(const RoomData& other) const
{
	return _id == other._id && _isActive == other._isActive && _maxPlayers == other._maxPlayers
		&& _name == other._name && _timePerQuestion == other._timePerQuestion && _questionCount == other._questionCount;
}
