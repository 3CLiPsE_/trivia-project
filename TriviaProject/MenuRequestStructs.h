#pragma once

#include <string>


struct GetPlayersInRoomRequest
{
	unsigned int _roomId;

	GetPlayersInRoomRequest(unsigned int roomId);
};

struct JoinRoomRequest
{
	unsigned int _roomId;

	JoinRoomRequest(unsigned int roomId);
};

struct CreateRoomRequest
{
	std::string _name;
	unsigned int _maxUsers;
	unsigned int _questionCount;
	unsigned int _answerTimeout;

	CreateRoomRequest(const std::string& name, unsigned int maxUsers, unsigned int questionCount, unsigned int answerTimeout);
};
