#include "RoomMemberHandler.h"
#include "MyException.h"
#include "JsonResponsePacketSerializer.h"
#include "Helper.h"


RoomMemberHandler::RoomMemberHandler(Room* room, const LoggedUser& user, RoomManager& roomManager, RequestHandlerFactory* handlerFactory) :
	IRoomRequestHandler(roomManager, room, user), _handlerFactory(handlerFactory)
{

}
RoomMemberHandler::~RoomMemberHandler()
{
}

bool RoomMemberHandler::isRequestRelevant(const Request& request)
{
	return request._requestId == GET_ROOM_STATE_REQUEST_CODE || request._requestId == LEAVE_ROOM_REQUEST_CODE;
}
RequestResult RoomMemberHandler::handleRequest(const Request& request)
{
	switch (request._requestId)
	{
	case GET_ROOM_STATE_REQUEST_CODE:
		return getRoomState(request);
		break;
	case LEAVE_ROOM_REQUEST_CODE:
		return leaveRoom(request);
		break;
	default:
		throw MyException("Invalid Request Type");
		break;
	}
}

RequestResult RoomMemberHandler::leaveRoom(const Request& request)
{
	_room->removeUser(_user);
	std::vector<LoggedUser> users = _room->getAllUsers();

	Helper::addNotificationToQueue(GET_ROOM_STATE_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse(REQUEST_SUCCESSFUL, _room)), users, _handlerFactory);

	return RequestResult(LEAVE_ROOM_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse(REQUEST_SUCCESSFUL)), _handlerFactory->createMenuRequestHandler(_user));
}
RequestResult RoomMemberHandler::getRoomState(const Request& request)
{
	return RequestResult(GET_ROOM_STATE_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse(REQUEST_SUCCESSFUL, _room)),
		nullptr);
}
