#include "GameManager.h"
#include "MyException.h"
#include "Sqlite.h"


std::mutex GameManager::_managerMutex;

GameManager::GameManager(IDatabase& database) : _database(database)
{

}

Game* GameManager::CreateGame(Room* room)
{
	std::lock_guard<std::mutex> guard(_managerMutex);

	//if the game exists return it else create a new game
	std::vector<Game*>::iterator targetGame = std::find_if(_games.begin(), _games.end(), [room](Game* game) {
		return game->getOriginRoomId() == room->getRoomData()._id; });
	if (targetGame != _games.end())
		return *targetGame;

	int gameId  = _database.startGame();
	_games.push_back(new Game(gameId, room->getRoomData()._id, room->getRoomData()._timePerQuestion, _database.getQuestions(room->getRoomData()._questionCount), room->getAllUsers()));
	return _games.back();
}
void GameManager::DeleteGame(Game* game)
{
	std::lock_guard<std::mutex> guard(_managerMutex);

    _database.endGame(game->getId());
	delete game;
	_games.erase(std::remove(_games.begin(), _games.end(), game), _games.end());
}
void GameManager::addStatistic(const Game& game, const LoggedUser& user)
{
	GameData userData = game.getGameDataForUser(user);

	int questionCount = userData._correctAnswerCount + userData._wrongAnswerCount;
	int totalTime = userData._averageAnswerTime * questionCount;
	_database.addStatistic(user.getUsername(), game.getId(), userData._correctAnswerCount, userData._wrongAnswerCount, totalTime, questionCount);
}
