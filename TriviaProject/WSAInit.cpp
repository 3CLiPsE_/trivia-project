#include "WSAInit.h"
#include <exception>


WSAInit::WSAInit()
{
	WSADATA wsa_data = { };
	if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
		throw std::exception("WSAStartup Failed");
}

WSAInit::~WSAInit()
{
	try
	{
		WSACleanup();
	}
	catch (...) {}
}
