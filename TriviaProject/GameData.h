#pragma once

#include "Question.h"


struct GameData
{
	unsigned int _questionIndex;
	unsigned int _correctAnswerCount;
	unsigned int _wrongAnswerCount;
	unsigned int _averageAnswerTime;

	GameData(unsigned int questionIndex);
	GameData(unsigned int questionIndex, unsigned int correntAnswerCount, unsigned int wrongAnswerCount, unsigned int averageAnswerTime);

	void addAnswer(bool isCorrect, unsigned int answerTime);

private:
	std::vector<unsigned int> _answerTimes;
};
