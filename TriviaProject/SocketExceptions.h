#pragma once

#include <exception>


class SocketDisconnectedException : public std::exception
{
public:
	SocketDisconnectedException() = default;
	virtual ~SocketDisconnectedException() noexcept = default;
	virtual const char* what() const noexcept { return "Socket Disconnected"; }
};
