#pragma once

#include <string>
#include <Windows.h>
#include "ProtocolCodes.h"


struct ErrorResponse
{
	std::string _msg;
	const BYTE _code = REQUEST_UNSUCCESSFUL;

	ErrorResponse(const std::string& msg);
};
