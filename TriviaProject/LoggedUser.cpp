#include "LoggedUser.h"


LoggedUser::LoggedUser(const std::string& username, unsigned int id)
	: _username(username), _id(id)
{

}
LoggedUser::LoggedUser(const LoggedUser& other) :
	_username(other._username), _id(other._id)
{

}

std::string LoggedUser::getUsername() const
{
	return _username;
}
unsigned int LoggedUser::getId() const
{
	return _id;
}
bool LoggedUser::operator==(const LoggedUser& other) const
{
	return _username == other._username && _id == other._id;
}
bool LoggedUser::operator!=(const LoggedUser& other) const
{
	return !operator==(other);
}
