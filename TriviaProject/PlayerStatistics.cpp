#include "PlayerStatistics.h"


PlayerStatistics::PlayerStatistics(int correctAnsCount, int wrongAnsCount, double avgAnsCount, int numberOfGames, const std::string& name) :
	_correctAnsCount(correctAnsCount), _wrongAnsCount(wrongAnsCount), _avgAnsTime(avgAnsCount), _numberOfGames(numberOfGames), _name(name)
{

}
