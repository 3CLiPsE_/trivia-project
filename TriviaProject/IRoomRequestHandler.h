#pragma once

#include "IRequestHandler.h"
#include "Room.h"
#include "RoomManager.h"


class IRoomRequestHandler : public IRequestHandler
{
public:
	IRoomRequestHandler(RoomManager& roomManager, Room* room, const LoggedUser& user);

protected:
	Room* _room;
	LoggedUser _user;
	RoomManager& _roomManager;

	RequestResult getRoomState(const Request& request);
};
