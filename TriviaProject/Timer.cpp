#include "Timer.h"
#include <thread>


__event void Timer<T>::tickEvent()
{
	auto startTime = std::chrono::system_clock::now();
	while (!_stop && std::chrono::system_clock::now() - startTime < _time)
	{
		std::this_thread::sleep_for(std::chrono::microseconds(10)); //in order to not overload the cpu
	}
}

void Timer::start()
{
	_stop = false;
	__raise tickEvent;
}
void Timer::stop()
{
	_stop = true;
}