#include "RequestResult.h"
#include "Helper.h"


RequestResult::RequestResult(BYTE code, const std::vector<char>& buffer, IRequestHandler* requestHandler)
{
	_newHandler = requestHandler;

	std::vector<unsigned char> bufferSize = Helper::intToBytes((int)buffer.size());

	_response.insert(_response.end(), &code, (&code) + sizeof(BYTE));
	_response.insert(_response.end(), bufferSize.begin(), bufferSize.end());
	_response.insert(_response.end(), buffer.begin(), buffer.end());
}

RequestResult::RequestResult() : _newHandler(nullptr), _response()
{

}
