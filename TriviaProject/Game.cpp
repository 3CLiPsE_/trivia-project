#include "Game.h"
#include "MyException.h"
#include "Helper.h"
#include "JsonResponsePacketSerializer.h"
#include <stdexcept>
#include <thread>


Game::Game(unsigned int id, unsigned int roomId, unsigned int timeForQuestion, std::vector<Question> questions, std::vector<LoggedUser> players)
	: _questions(questions), _originRoomId(roomId), _id(id), _nextQuestion(true), _loggedUsers(players)
{
	for (auto user : players)
		_players.insert({ user, GameData(0) });

	std::thread notificationThread(&Game::manageTimerForGame, this, timeForQuestion);
	notificationThread.detach();
}
Game::~Game()
{

}

void Game::manageTimerForGame(unsigned int timeForQuestion)
{
	for (unsigned int i = 0; i < _questions.size() && !_players.empty(); i++)
	{
		std::unique_lock<std::mutex> didntAnswerLock(_didntAnswerMutex);
		if (_didntAnswerPlayers.size() != _loggedUsers.size())
			_didntAnswerCv.wait(didntAnswerLock, [this]() {return _didntAnswerPlayers.size() == _loggedUsers.size(); }); //all players got the questions

		_nextQuestion = false;

		bool finishedWaiting = _didntAnswerCv.wait_until(didntAnswerLock, std::chrono::system_clock::now() + std::chrono::seconds(timeForQuestion),
			[this]() {return _didntAnswerPlayers.size() == 0; }); //wait_until returns false when the timeout was reached

		if (!finishedWaiting)
		{
			Helper::addNotificationToQueue(TIME_OUT_RESPONSE_CODE, std::vector<char>(), _didntAnswerPlayers, nullptr);
			for (auto& player : _didntAnswerPlayers)
				_players.at(player).addAnswer(false, timeForQuestion);
		}

		_didntAnswerPlayers = std::vector<LoggedUser>();
		std::unique_lock<std::mutex> gameLock(_gameMutex);
		//advance everyone to the next question
		for (auto& player : _loggedUsers)
		{
			_players.at(player)._questionIndex = i + 1;
		}

		_nextQuestion = true;
		_nextQuestionCV.notify_all();
	}
}
Question Game::getCurrentQuestionForUser(const LoggedUser & user)
{
	try
	{
		return _questions.at(_players.at(user)._questionIndex);
	}
	catch (std::out_of_range&)
	{
		throw MyException("No more Questions Remaine");
	}
}
Question Game::getQuestionForUser(const LoggedUser & user)
{
	if (!_nextQuestion)
	{
		std::unique_lock<std::mutex> lock(_nextQuestionMutex);
		_nextQuestionCV.wait(lock, [this]() {return _nextQuestion; });
	}

	Question currentQuestion("", "", std::vector<std::string>());
	try
	{
		currentQuestion = _questions.at(_players.at(user)._questionIndex);

		std::lock_guard<std::mutex> guard(_didntAnswerMutex);
		_didntAnswerPlayers.push_back(user);
		if (_didntAnswerPlayers.size() == _loggedUsers.size())
			_didntAnswerCv.notify_all();
	}
	catch (std::out_of_range&)
	{
		throw MyException("No more Questions Remaine");
	}

	return currentQuestion;
}
void Game::submitAnswer(const LoggedUser & user, unsigned int answerId, unsigned int answerTime)
{
	std::unique_lock<std::mutex> gameLock(_gameMutex);
	_players.at(user).addAnswer(
		answerId == _questions[_players.at(user)._questionIndex].getCorrectAnswerId(),
		answerTime);

	std::unique_lock<std::mutex> didntAnswerLock(_didntAnswerMutex);
	_didntAnswerPlayers.erase(std::remove(_didntAnswerPlayers.begin(), _didntAnswerPlayers.end(), user), _didntAnswerPlayers.end());
	if (_didntAnswerPlayers.empty())
		_didntAnswerCv.notify_all();

}
void Game::removePlayer(const LoggedUser & user, bool answered)
{
	if (!answered)
	{
		std::unique_lock<std::mutex> didntAnswerLock(_didntAnswerMutex);
		_didntAnswerPlayers.erase(std::remove(_didntAnswerPlayers.begin(), _didntAnswerPlayers.end(), user), _didntAnswerPlayers.end());
		if (_didntAnswerPlayers.empty())
			_didntAnswerCv.notify_all();
	}

	std::unique_lock<std::mutex> gameLock(_gameMutex);
	_loggedUsers.erase(std::remove(_loggedUsers.begin(), _loggedUsers.end(), user), _loggedUsers.end());
}
std::unordered_map<LoggedUser, GameData> Game::getPlayers() const
{
	return _players;
}
GameData Game::getGameDataForUser(const LoggedUser & user) const
{
	return _players.at(user);
}
unsigned int Game::getId() const
{
	return _id;
}
unsigned int Game::getOriginRoomId() const
{
	return _originRoomId;
}
bool Game::operator==(const Game & other)
{
	return _id == other._id;
}
bool Game::operator!=(const Game & other)
{
	return !operator==(other);
}
bool Game::gameEmpty() const
{
	return _loggedUsers.empty();
}
