#include "MenuResponseStructs.h"


LogoutResponse::LogoutResponse(unsigned int status) : _status(status)
{

}

GetRoomsRsponse::GetRoomsRsponse(unsigned int status, const std::vector<Room*>& rooms) :
	_status(status)
{
	for (auto room : rooms)
		_rooms.push_back(room->getRoomData());
}

GetPlayersInRoomResponse::GetPlayersInRoomResponse(unsigned int status, const std::vector<LoggedUser>& players) :
	_status(status)
{
	for (auto player : players)
		_players.push_back(player.getUsername());
}

HighscoreResponse::HighscoreResponse(unsigned int status, const std::map<std::string, int>& highscores) :
	_status(status), _highscores(highscores)
{
	
}

JoinRoomResponse::JoinRoomResponse(unsigned int status) : _status(status)
{

}

CreateRoomResponse::CreateRoomResponse(unsigned int status, unsigned int roomId) : _status(status), _roomId(roomId)
{
	
}

StatisticResponse::StatisticResponse(unsigned int status, const PlayerStatistics& statistics) :
	_correctAnsCount(statistics._correctAnsCount), _wrongAnsCount(statistics._wrongAnsCount), _avgAnsTime(statistics._avgAnsTime), _status(status)
{

}
