#pragma once

#include <string>


struct LoginRequest
{
	std::string _username;
	std::string _password;

	LoginRequest(const std::string& username, const std::string& password);
};

struct SignupRequest
{
	std::string _username;
	std::string _password;
	std::string _email;

	SignupRequest(const std::string& username, const std::string& password, const std::string& email);
};
