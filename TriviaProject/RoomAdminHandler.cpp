#include "RoomAdminHandler.h"
#include "MyException.h"
#include "JsonResponsePacketSerializer.h"
#include "Helper.h"


RoomAdminHandler::RoomAdminHandler(Room* room, const LoggedUser& user, RoomManager& roomManager, RequestHandlerFactory* handlerFactory) :
	_handlerFactory(handlerFactory), IRoomRequestHandler(roomManager, room, user)
{
}
RoomAdminHandler::~RoomAdminHandler()
{
}

bool RoomAdminHandler::isRequestRelevant(const Request& request)
{
	return request._requestId == CLOSE_ROOM_REQUEST_CODE ||
		request._requestId == START_GAME_REQUEST_CODE ||
		request._requestId == GET_ROOM_STATE_REQUEST_CODE;
}
RequestResult RoomAdminHandler::handleRequest(const Request& request)
{
	switch (request._requestId)
	{
	case CLOSE_ROOM_REQUEST_CODE:
		return closeRoom(request);
	case START_GAME_REQUEST_CODE:
		return startGame(request);
	case GET_ROOM_STATE_REQUEST_CODE:
		return getRoomState(request);
	default:
		throw MyException("Invalid Request Type");
	}
}
RequestResult RoomAdminHandler::closeRoom(const Request& request)
{
	std::vector<LoggedUser> users = _room->getAllUsers();
	users.erase(std::remove(users.begin(), users.end(), _user), users.end());

	Helper::addNotificationToQueue(CLOSE_ROOM_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse(REQUEST_SUCCESSFUL)), users, _handlerFactory);

	_roomManager.DeleteRoom(_room->getRoomData()._id);

	return RequestResult(CLOSE_ROOM_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse(REQUEST_SUCCESSFUL)),
		_handlerFactory->createMenuRequestHandler(_user));
}
RequestResult RoomAdminHandler::startGame(const Request& request)
{
	std::vector<LoggedUser> users = _room->getAllUsers();
	users.erase(std::remove(users.begin(), users.end(), _user), users.end());

	Helper::addNotificationToQueue(START_GAME_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(StartRoomResponse(REQUEST_SUCCESSFUL)), users, _handlerFactory, _room);

	_room->startGame();
	return RequestResult(START_GAME_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(StartRoomResponse(REQUEST_SUCCESSFUL)),
		_handlerFactory->createGameRequestHandler(_room, _user));	// pass handler for game
}
