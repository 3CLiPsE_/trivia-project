#pragma once

#include "sqlite3.h"
#include "SqliteException.h"


class Sqlite
{
public:
	Sqlite(const char* fileName);
	~Sqlite();

	Sqlite& createQuery(const std::string& query); //create the qurry
	bool runQuery(); //run the qurry and return if a row is available

	//bind the data
	Sqlite& operator<< (int value);
	Sqlite& operator<< (double value);
	Sqlite& operator<< (const std::string& value);
	Sqlite& operator<< (long long value);

	//get row data
	Sqlite& operator>> (int& other);
	Sqlite& operator>> (double& other);
	Sqlite& operator>> (std::string& other);
	Sqlite& operator>> (long long& other);

	void skip(size_t num)
	{
		_currentColumnIndex += num;
	}

private:
	sqlite3* _db;
	sqlite3_stmt* _statment;
	size_t _currentParamIndex;
	size_t _currentColumnIndex;
};
