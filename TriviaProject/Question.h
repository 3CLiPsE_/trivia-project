#pragma once

#include <string>
#include <vector>


class Question
{	
public:
	Question(const std::string& question, const std::string& correct, const std::vector<std::string>& answers);
	Question(const Question& question);
	~Question();

	std::vector<std::string> getPossibleAnswers() const;
	std::string getQuestion() const;
	int getCorrectAnswerId() const;

private:
	std::string _question;
	std::vector<std::string> _answers;
	int _correct;
};
