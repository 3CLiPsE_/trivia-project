#pragma once

#include <string>
#include <map>


struct RoomData
{
	unsigned int _id;
	std::string _name;
	unsigned int _maxPlayers;
	unsigned int _timePerQuestion;
	unsigned int _questionCount;
	bool _isActive;

	RoomData(unsigned int id, const std::string& name, unsigned int maxPlayers, unsigned int timePerQuestion, unsigned int questionCount, bool isActive);
	
	bool operator==(const RoomData& other) const;
};
