#include "MenuRequestHandler.h"
#include "MyException.h"
#include "ProtocolCodes.h"
#include "MenuResponseStructs.h"
#include "MenuRequestStructs.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "Helper.h"
#include <algorithm>
#include <iostream>


MenuRequestHandler::MenuRequestHandler(RoomManager& roomManager, LoginManager& loginManager, RequestHandlerFactory* handlerFactory, const LoggedUser& user, const HighscoreTable& highscoreTable) :
	_roomManager(roomManager), _loginManager(loginManager), _handlerFactory(handlerFactory), _user(user), _highscroeTable(highscoreTable)
{

}

MenuRequestHandler::~MenuRequestHandler()
{

}

bool MenuRequestHandler::isRequestRelevant(const Request& request)
{
	return request._requestId == SIGNOUT_REQUEST_CODE || request._requestId == GET_ROOMS_REQUEST_CODE ||
		request._requestId == GET_PLAYERS_REQUEST_CODE || request._requestId == GET_HIGHSCORES_REQUEST_CODE ||
		request._requestId == JOIN_REQUEST_CODE || request._requestId == CREATE_REQUEST_CODE ||
		request._requestId == GET_PLAYER_STATISTICS_REQUEST_CODE;
}
RequestResult MenuRequestHandler::handleRequest(const Request& request)
{
	switch (request._requestId)
	{
	case SIGNOUT_REQUEST_CODE:
		return signout(request);
		break;
	case GET_ROOMS_REQUEST_CODE:
		return getRooms(request);
		break;
	case GET_PLAYERS_REQUEST_CODE:
		return getPlayersInRoom(request);
		break;
	case GET_HIGHSCORES_REQUEST_CODE:
		return getHighscores(request);
		break;
	case JOIN_REQUEST_CODE:
		return joinRoom(request);
		break;
	case CREATE_REQUEST_CODE:
		return createRoom(request);
		break;
	case GET_PLAYER_STATISTICS_REQUEST_CODE:
		return getPlayerStatistics(request);
		break;
	default:
		throw MyException("Invalid Request");
		break;
	}
}
RequestResult MenuRequestHandler::signout(const Request& request)
{
	_loginManager.logout(_user.getUsername());

	return RequestResult(SIGNOUT_REQUEST_CODE, std::vector<char>(), nullptr);
}
RequestResult MenuRequestHandler::getRooms(const Request& request)
{
	return RequestResult(GET_ROOMS_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(GetRoomsRsponse(REQUEST_SUCCESSFUL, _roomManager.getRooms())),
		nullptr);
}
RequestResult MenuRequestHandler::getPlayersInRoom(const Request& request)
{
	GetPlayersInRoomRequest getPlayersRequest = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(request._buffer);
	int id = getPlayersRequest._roomId;
	std::vector<Room*> rooms = _roomManager.getRooms();

	std::vector<Room*>::iterator targetRoom = std::find_if(rooms.begin(), rooms.end(), [id](Room * room) {return room->getRoomData()._id == id; });

	if (targetRoom != rooms.end())
		return RequestResult(GET_PLAYERS_REQUEST_CODE,
			JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse(REQUEST_SUCCESSFUL, (*targetRoom)->getAllUsers())), nullptr);


	return Helper::getErrorResult(request._requestId, "Invalid Room Id");
}
RequestResult MenuRequestHandler::getHighscores(const Request & request)
{
	return RequestResult(GET_HIGHSCORES_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(HighscoreResponse(REQUEST_SUCCESSFUL, _highscroeTable.getHighscores())),
		nullptr);
}
RequestResult MenuRequestHandler::joinRoom(const Request & request)
{
	JoinRoomRequest joinRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(request._buffer);
	int id = joinRequest._roomId;

	try
	{
		_roomManager.AddUserToRoom(id, _user);
	}
	catch (std::exception & e)
	{
		return Helper::getErrorResult(request._requestId, e.what());
	}

	Room* targetRoom = _roomManager.getRoomById(id);
	std::vector<LoggedUser> users = targetRoom->getAllUsers();
	users.erase(std::remove(users.begin(), users.end(), _user), users.end());

	Helper::addNotificationToQueue(GET_ROOM_STATE_REQUEST_CODE, JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse(REQUEST_SUCCESSFUL, targetRoom)), users, _handlerFactory);

	return RequestResult(JOIN_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse(REQUEST_SUCCESSFUL)),
		_handlerFactory->createRoomMemberRequestHandler(targetRoom, _user));
}
RequestResult MenuRequestHandler::createRoom(const Request & request)
{
	CreateRoomRequest createRequest = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(request._buffer);
	try
	{
		int roomId = _roomManager.CreateRoom(_user, createRequest._name, createRequest._answerTimeout, createRequest._questionCount, createRequest._maxUsers);
		return RequestResult(CREATE_REQUEST_CODE,
			JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse(REQUEST_SUCCESSFUL, roomId)),
			_handlerFactory->createRoomAdminRequestHandler(_roomManager.getRoomById(roomId), _user));
	}
	catch (MyException & e)
	{
		return Helper::getErrorResult(request._requestId, e.what());
	}
}
RequestResult MenuRequestHandler::getPlayerStatistics(const Request & request)
{
	return RequestResult(GET_PLAYER_STATISTICS_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(StatisticResponse(REQUEST_SUCCESSFUL, _highscroeTable.getPlayerStatistics(_user.getUsername()))),
		nullptr);
}
