#pragma once

#include "LoginRequestStructs.h"
#include "MenuRequestStructs.h"
#include "GameRequestStructs.h"
#include <vector>

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest  deserializeLoginRequest(const std::vector<char>& buffer);
	static SignupRequest deserializeSignupRequest(const std::vector<char>& buffer);

	static GetPlayersInRoomRequest deserializeGetPlayersRequest(const std::vector<char>& buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(const std::vector<char>& buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(const std::vector<char>& buffer);
	
	static SubmitAnswerRequest deserializeSumbitAnswerRequest(const std::vector<char>& buffer);
};
