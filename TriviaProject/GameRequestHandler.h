#pragma once

#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"


class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(GameManager& gameManager, RoomManager& roomManager, Game* game, const LoggedUser& user, RequestHandlerFactory* handlerFactory);

	bool isRequestRelevant(const Request& request);
	RequestResult handleRequest(const Request& request);

private:
	GameManager& _gameManager;
	RoomManager& _roomManager;
	Game* _game;
	LoggedUser _user;
	RequestHandlerFactory* _handlerFactory;
	CTime _lastQuestionTime;
	bool _hasQuestion;

	RequestResult getQuestion(const Request& request);
	RequestResult submitAnswer(const Request& request);
	RequestResult getGameResults(const Request& request);
	RequestResult leaveGame(const Request& request);
	void exitGame();
};
