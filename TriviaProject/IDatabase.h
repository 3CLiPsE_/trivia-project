#pragma once

#include <map>
#include <list>
#include "LoggedUser.h"
#include "Question.h"
#include "PlayerStatistics.h"


class IDatabase
{
public:
	virtual void addUser(const std::string& username, const std::string& password, const std::string& email) = 0;
	virtual bool matchPassword(const std::string& username, const std::string& password) = 0;
	virtual void addStatistic(const std::string& username, int game_id, int correctAns, int wrongAns, int totalTime, int questionCount) = 0;
	virtual std::map<std::string, int> getHighScores() = 0;
	virtual PlayerStatistics getPlayerStatistics(const std::string& name) = 0;
	virtual bool doesUserExist(const std::string& username) = 0;
	virtual unsigned int getQuestionCount() = 0;
	virtual std::vector<Question> getQuestions(unsigned int count) = 0;
	virtual int startGame() = 0; // returns the new game id
	virtual void endGame(int gameId) = 0;
};
