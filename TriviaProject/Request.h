#pragma once

#include <atltime.h>
#include <string>
#include <vector>


struct Request
{
	int _requestId;
	CTime _recivalTime;
	std::vector<char> _buffer;

	Request(int requestId, const CTime& recivalTime, const std::vector<char>& buffer);
	Request(int requestId, const std::vector<char>& buffer);
	bool isSignout();
};
