#include "MenuRequestStructs.h"


GetPlayersInRoomRequest::GetPlayersInRoomRequest(unsigned int roomId) : _roomId(roomId)
{

}

JoinRoomRequest::JoinRoomRequest(unsigned int roomId) : _roomId(roomId)
{

}

CreateRoomRequest::CreateRoomRequest(const std::string& name, unsigned int maxUsers, unsigned int questionCount, unsigned int answerTimeout) :
	_name(name), _maxUsers(maxUsers), _questionCount(questionCount), _answerTimeout(answerTimeout)
{

}
