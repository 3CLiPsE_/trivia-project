#include "Sqlite.h"
#include "SqliteException.h"
#include <string>

#define INT_PARAM
#define STRING_PARAM

Sqlite::Sqlite(const char* fileName) : _statment(nullptr), _currentColumnIndex(0), _currentParamIndex(1) //indices start from 1
{
	sqlite3_open(fileName, &_db);
}
Sqlite::~Sqlite()
{
	if (_statment != nullptr)
		sqlite3_finalize(_statment);

	sqlite3_close(_db);
}

Sqlite& Sqlite::createQuery(const std::string & query)
{
	if (_statment != nullptr)
	{
		sqlite3_finalize(_statment);
		_statment = nullptr;
	}
	_currentParamIndex = 1; //reseting the param index

	if (sqlite3_prepare_v2(_db, query.c_str(), query.size(), &_statment, nullptr) != SQLITE_OK)
		throw SqliteException(sqlite3_errmsg(_db));
	return *this;
}
bool Sqlite::runQuery()
{
	_currentColumnIndex = 0; // reseting the column index
	bool res = (sqlite3_step(_statment) == SQLITE_ROW);
	return res;
}
Sqlite & Sqlite::operator<< (int value)
{
	int resultCode = sqlite3_bind_int(_statment, _currentParamIndex++, value);
	if (resultCode != SQLITE_OK)
		throw SqliteException(sqlite3_errstr(resultCode));

	return *this;
}
Sqlite& Sqlite::operator<< (double value)
{
	int resultCode = sqlite3_bind_double(_statment, _currentParamIndex++, value);
	if (resultCode != SQLITE_OK)
		throw SqliteException(sqlite3_errstr(resultCode));

	return *this;
}
Sqlite& Sqlite::operator<< (const std::string & value)
{
	int resultCode = sqlite3_bind_text(_statment, _currentParamIndex++, value.c_str(), value.size(), SQLITE_STATIC);
	if (resultCode != SQLITE_OK)
		throw SqliteException(sqlite3_errstr(resultCode));

	return *this;
}
Sqlite& Sqlite::operator<< (long long value)
{
	int resultCode = sqlite3_bind_int64(_statment, _currentParamIndex++, value);
	if (resultCode != SQLITE_OK)
		throw SqliteException(sqlite3_errstr(resultCode));

	return *this;
}
Sqlite& Sqlite::operator>> (int& other)
{
	int type = sqlite3_column_type(_statment, _currentColumnIndex);
	if (type != SQLITE_INTEGER)
		throw SqliteTypeException("target type shouldn't be int", SQLITE_INTEGER, type);

	other = sqlite3_column_int(_statment, _currentColumnIndex++);
	return *this;
}
Sqlite& Sqlite::operator>> (double& other)
{
	int type = sqlite3_column_type(_statment, _currentColumnIndex);
	if (type != SQLITE_FLOAT)
		throw SqliteTypeException("target type shouldn't be double", SQLITE_FLOAT, type);

	other = sqlite3_column_double(_statment, _currentColumnIndex++);
	return *this;
}
Sqlite& Sqlite::operator>> (std::string & other)
{
	int type = sqlite3_column_type(_statment, _currentColumnIndex);
	if (type != SQLITE_TEXT)
		throw SqliteTypeException("target type shouldn't be string", SQLITE_TEXT, type);

	other = std::string((const char*)sqlite3_column_text(_statment, _currentColumnIndex++));
	return *this;
}
Sqlite& Sqlite::operator>> (long long& other)
{
	int type = sqlite3_column_type(_statment, _currentColumnIndex);
	if (type != SQLITE_INTEGER)
		throw SqliteTypeException("target type shouldn't be long", SQLITE_INTEGER, type);

	other = sqlite3_column_int64(_statment, _currentColumnIndex++);
	return *this;
}
