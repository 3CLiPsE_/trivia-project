#include "GameRequestHandler.h"
#include "ProtocolCodes.h"
#include "MyException.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "Helper.h"


GameRequestHandler::GameRequestHandler(GameManager& gameManager, RoomManager& roomManager, Game* game, const LoggedUser& user, RequestHandlerFactory* handlerFactory) :
	_gameManager(gameManager), _roomManager(roomManager), _game(game), _user(user), _handlerFactory(handlerFactory), _hasQuestion(false)
{

}

bool GameRequestHandler::isRequestRelevant(const Request& request)
{
	return request._requestId == GET_GAME_RESULTS_REQUEST_CODE ||
		request._requestId == GET_QUESTION_REQUEST_CODE ||
		request._requestId == SUBMIT_ANSWER_REQUEST_CODE ||
		request._requestId == LEAVE_GAME_REQUEST_CODE;
}
RequestResult GameRequestHandler::handleRequest(const Request& request)
{
	switch (request._requestId)
	{
	case GET_GAME_RESULTS_REQUEST_CODE:
		return getGameResults(request);
		break;
	case GET_QUESTION_REQUEST_CODE:
		return getQuestion(request);
		break;
	case SUBMIT_ANSWER_REQUEST_CODE:
		return submitAnswer(request);
		break;
	case LEAVE_GAME_REQUEST_CODE:
		return leaveGame(request);
		break;
	default:
		throw MyException("Unsupported msg");
	}
}

RequestResult GameRequestHandler::getQuestion(const Request& request)
{
	if (_hasQuestion)
		return Helper::getErrorResult(GET_QUESTION_REQUEST_CODE, "already have a question");

	Question requestedQuestion("", "", std::vector<std::string>());
	try
	{
		requestedQuestion = _game->getQuestionForUser(_user);
	}
	catch (const MyException & e)
	{
		return Helper::getErrorResult(request._requestId, e.what());
	}
	_lastQuestionTime = CTime::GetCurrentTime();
	_hasQuestion = true;

	return RequestResult(GET_QUESTION_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse(REQUEST_SUCCESSFUL, requestedQuestion))
		, nullptr);
}
RequestResult GameRequestHandler::submitAnswer(const Request& request)
{
	SubmitAnswerRequest submitRequest = JsonRequestPacketDeserializer::deserializeSumbitAnswerRequest(request._buffer);

	if (!_hasQuestion)
		return Helper::getErrorResult(request._requestId, "Cannot answer before the question was sent.");

	if (submitRequest._answerId < 0 || submitRequest._answerId > 3)
		return Helper::getErrorResult(request._requestId, "Invalid answer.");
	_game->submitAnswer(_user, submitRequest._answerId, (size_t)(CTime::GetCurrentTime() - _lastQuestionTime).GetTotalSeconds());
	Question currentQuestion = _game->getCurrentQuestionForUser(_user);
	_hasQuestion = false;

	return RequestResult(SUBMIT_ANSWER_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse(REQUEST_SUCCESSFUL, currentQuestion.getCorrectAnswerId()))
		, nullptr);
}
RequestResult GameRequestHandler::getGameResults(const Request & request)
{
	std::unordered_map<LoggedUser, GameData> players = _game->getPlayers();

	exitGame();

	return RequestResult(GET_GAME_RESULTS_REQUEST_CODE,
		JsonResponsePacketSerializer::serializeResponse(GetGameResultResponse(REQUEST_SUCCESSFUL, players)),
		_handlerFactory->createMenuRequestHandler(_user));
}
RequestResult GameRequestHandler::leaveGame(const Request & request)
{
	exitGame();

	return RequestResult(LEAVE_GAME_REQUEST_CODE, std::vector<char>(), _handlerFactory->createMenuRequestHandler(_user));
}
void GameRequestHandler::exitGame()
{
	_gameManager.addStatistic(*_game, _user);
	_game->removePlayer(_user, !_hasQuestion); // if the player has a question it means he didn't answer it

	if (_game->gameEmpty())
	{
		_roomManager.DeleteRoom(_game->getOriginRoomId());
		_gameManager.DeleteGame(_game);
	}
}
