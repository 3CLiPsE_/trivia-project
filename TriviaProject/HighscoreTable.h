#pragma once

#include "IDatabase.h"


struct HighscoreTable
{
public:
	HighscoreTable(IDatabase& database);

	void insertScore(const std::string& user, int game_id, int correctAns, int wrongAns, int totalAns, int totalAnsTime);
	std::map<std::string, int> getHighscores() const;
	PlayerStatistics getPlayerStatistics(const std::string& user) const;

private:
	IDatabase& _database;
};
