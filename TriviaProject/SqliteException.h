#pragma once

#include <exception>
#include <string>


class SqliteException : public std::exception
{
public:
	SqliteException(const std::string& errorMsg);
	
	const char* what() const noexcept;

protected:
	std::string _msg;
};

class SqliteTypeException : public SqliteException
{
public:
	SqliteTypeException(const std::string& errorMsg, int expected, int got);
	
	const char* what() const noexcept;
	int Expected() const;
	int Got() const;

private:
	int _expected, _got;
};
