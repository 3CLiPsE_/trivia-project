#include "LoginRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "LoginResponseStructs.h"
#include "MyException.h"
#include "Helper.h"


LoginRequestHandler::LoginRequestHandler(LoginManager& loginManager, RequestHandlerFactory* handlerFactroy, unsigned int userId) :
	_loginManger(loginManager), _handlerFactory(handlerFactroy), _userId(userId)
{

}

RequestResult LoginRequestHandler::login(const Request& request)
{
	LoginRequest loginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(request._buffer);
	LoggedUser user(loginRequest._username, _userId);

	try
	{
		_loginManger.login(user, loginRequest._password);
	}
	catch (const std::exception & e)
	{
		return Helper::getErrorResult(request._requestId, e.what());
	}

	return RequestResult(LOGIN_REQUEST_CODE, JsonResponsePacketSerializer::serializeResponse(LoginResponse(REQUEST_SUCCESSFUL)), _handlerFactory->createMenuRequestHandler(user));
}
RequestResult LoginRequestHandler::signup(const Request& request)
{
	SignupRequest signupRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(request._buffer);
	LoggedUser user(signupRequest._username, _userId);

	try
	{
		_loginManger.signup(user, signupRequest._password, signupRequest._email);
	}
	catch (std::exception & e)
	{
		return Helper::getErrorResult(request._requestId, e.what());
	}
	return RequestResult(SIGNUP_REQUEST_CODE, JsonResponsePacketSerializer::serializeResponse(SignupResponse(REQUEST_SUCCESSFUL)), _handlerFactory->createMenuRequestHandler(user));
}

bool LoginRequestHandler::isRequestRelevant(const Request& request)
{
	return request._requestId == LOGIN_REQUEST_CODE || request._requestId == SIGNUP_REQUEST_CODE;
}
RequestResult LoginRequestHandler::handleRequest(const Request& request)
{
	switch (request._requestId)
	{
	case LOGIN_REQUEST_CODE:
		return login(request);
		break;
	case SIGNUP_REQUEST_CODE:
		return signup(request);
		break;
	default:
		throw MyException("Invalid Request Code");
	}
}
