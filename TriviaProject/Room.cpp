#include "Room.h"
#include "MyException.h"
#include <algorithm>


Room::Room(unsigned int id, const std::string& name, unsigned int maxPlayers, unsigned int timePerQuestion, unsigned int questionCount, bool isActive) :
	_metadata(id, name, maxPlayers, timePerQuestion, questionCount, isActive)
{

}
Room::~Room()
{
}

void Room::addUser(const LoggedUser& user)
{
	std::unique_lock<std::mutex> roomLock(_roomMutex);

	if (_users.size() < _metadata._maxPlayers)
		_users.push_back(user);
	else
		throw MyException("Room Full");
}
void Room::removeUser(const LoggedUser& user)
{
	std::unique_lock<std::mutex> roomLock(_roomMutex);

	_users.erase(std::remove(_users.begin(), _users.end(), user), _users.end());
}
std::vector<LoggedUser> Room::getAllUsers() const
{
	return _users;
}
RoomData Room::getRoomData() const
{
	return _metadata;
}
void Room::startGame()
{
	std::unique_lock<std::mutex> roomLock(_roomMutex);

	_metadata._isActive = true;

}
bool Room::operator==(const Room& other) const
{
	return _metadata == other._metadata && _users == other._users;
}
