#pragma once

#include <string>


struct LoginResponse
{
	unsigned int _status;

	LoginResponse(unsigned int status);
};

struct SignupResponse
{
	unsigned int _status;

	SignupResponse(unsigned int status);
};
