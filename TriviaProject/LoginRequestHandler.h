#pragma once

#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RequestHandlerFactory.h"
#include "ProtocolCodes.h"


class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(LoginManager& loginManager, RequestHandlerFactory* handlerFactroy, unsigned int userId);
	
	virtual bool isRequestRelevant(const Request& request);
	virtual RequestResult handleRequest(const Request& request);

private:
	LoginManager& _loginManger;
	RequestHandlerFactory* _handlerFactory;
	unsigned int _userId;

	RequestResult login(const Request& request);
	RequestResult signup(const Request& request);
};
