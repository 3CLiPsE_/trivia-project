#include "Server.h"


Server::Server(IDatabase& database) :
	_handlerFactory(database), _communicator(&_handlerFactory)
{

}
Server::~Server()
{

}

void Server::run()
{
	_communicator.bindAndListen();
	_communicator.handleRequests();
}
