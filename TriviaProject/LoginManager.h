#pragma once

#include "LoggedUser.h"
#include "IDatabase.h"
#include <vector>
#include <mutex>


class LoginManager
{
public:
	LoginManager(IDatabase& database);

	void signup(const LoggedUser& user, const std::string& password, const std::string& email);
	void login(const LoggedUser& user, const std::string& password);
	void logout(const std::string& username);

private:
	IDatabase& _database;
	std::vector<LoggedUser> _loggedUsers;
	static std::mutex _managerMutex;
};
