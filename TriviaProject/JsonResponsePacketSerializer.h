#pragma once

#include "LoginResponseStructs.h"
#include "ErrorResponse.h"
#include "MenuResponseStructs.h"
#include "RoomMemberStructs.h"
#include "GameResponseStructs.h"
#include <vector>


class JsonResponsePacketSerializer
{
public:
	static std::vector<char> serializeResponse(const ErrorResponse& response);

	static std::vector<char> serializeResponse(const LoginResponse& response);
	static std::vector<char> serializeResponse(const SignupResponse& response);
	static std::vector<char> serializeResponse(const LogoutResponse& response);
	
	static std::vector<char> serializeResponse(const HighscoreResponse& response);
	static std::vector<char> serializeResponse(const StatisticResponse& response);

	static std::vector<char> serializeResponse(const GetRoomsRsponse& response);
	static std::vector<char> serializeResponse(const GetPlayersInRoomResponse& response);
	static std::vector<char> serializeResponse(const JoinRoomResponse& response);
	static std::vector<char> serializeResponse(const CreateRoomResponse& response);
	static std::vector<char> serializeResponse(const LeaveRoomResponse& response);
	static std::vector<char> serializeResponse(const StartRoomResponse& response);
	static std::vector<char> serializeResponse(const CloseRoomResponse& response);
	static std::vector<char> serializeResponse(const GetRoomStateResponse& response);	 
	
	static std::vector<char> serializeResponse(const GetGameResultResponse& response);
	static std::vector<char> serializeResponse(const SubmitAnswerResponse& response);
	static std::vector<char> serializeResponse(const GetQuestionResponse& response);
};
