#include "SqliteDatabase.h"
#include "Question.h"
#include "MyException.h"
#include "SqliteException.h"
#include <algorithm>
#include <random>
#include <atltime.h>


#define DATABSE_NAME "TriviaDB.sqlite"
#define MAX_SCORES 5
#define NUMBER_OF_WRONG_ANSWERS 3

std::mutex SqliteDatabase::_dbMutex;

SqliteDatabase::SqliteDatabase() : _db(DATABSE_NAME)
{
	initDB();
}
SqliteDatabase::~SqliteDatabase()
{
}

void SqliteDatabase::initDB()
{
	executeQuery("CREATE TABLE IF NOT EXISTS User("
		"username TEXT PRIMARY KEY,"
		"password TEXT NOT NULL,"
		"email TEXT NOT NULL UNIQUE"
		");");

	executeQuery("CREATE TABLE IF NOT EXISTS Question("
		"question_id INTEGER PRIMARY KEY AUTOINCREMENT,"
		"question TEXT NOT NULL UNIQUE,"
		"correct_ans TEXT NOT NULL,"
		"ans2 TEXT NOT NULL,"
		"ans3 TEXT NOT NULL,"
		"ans4 TEXT NOT NULL"
		");");

	executeQuery("CREATE TABLE IF NOT EXISTS Game("
		"game_id INTEGER PRIMARY KEY AUTOINCREMENT,"
		"status INTEGER NOT NULL,"
		"start_time INTEGER NOT NULL,"
		"end_time INTEGER"
		");");

	executeQuery("CREATE TABLE IF NOT EXISTS Statistics("
		"id INTEGER PRIMARY KEY AUTOINCREMENT,"
		"username TEXT NOT NULL,"
		"game_id INTEGET NOT NULL,"
		"correct_ans_count INTEGER NOT NULL,"
		"wrong_ans_count INTEGER NOT NULL,"
		"question_count INTEGER NOT NULL CHECK(question_count>0)," //make sure the question num is positive
		"total_ans_time INTEGER NOT NULL CHECK(total_ans_time>0)," //make sure the time is positive
		"FOREIGN KEY(username) REFERENCES User(username),"
		"FOREIGN KEY(game_id) REFERENCES Game(game_id)"
		");");
}
bool SqliteDatabase::doesUserExist(const std::string& username)
{
	//specify a parameter place holder with '?'
	_db.createQuery("SELECT * FROM User WHERE username=?;") << username; //bind parameters using operator<<

	return _db.runQuery();	// runQuery is true if a row is waiting to be processed
}
unsigned int SqliteDatabase::getQuestionCount()
{

	unsigned int count = 0;
	_db.createQuery("SELECT question_id FROM Question;");
	while (_db.runQuery())
		count++;
	return count;
}
std::vector<Question> SqliteDatabase::getQuestions(unsigned int count)
{
	std::vector<Question> questions;
	_db.createQuery("SELECT * FROM Question;");
	while (_db.runQuery())
	{
		std::string question;
		std::string correct;
		std::string wrongAns;
		std::vector<std::string> answers;

		_db.skip(1); //skip the first value (the question id)
		_db >> question >> correct; //get the values of the column with operator>>
		//the values are according to the column creation order

		for (size_t i = 0; i < NUMBER_OF_WRONG_ANSWERS; i++)
		{
			_db >> wrongAns;
			answers.push_back(wrongAns);
		}

		questions.push_back(Question(question, correct, answers));
	}
	if (count > questions.size())
		throw MyException("Not enough questions in database.");

	auto rng = std::default_random_engine{};
	std::shuffle(questions.begin(), questions.end(), rng);
	return std::vector<Question>(questions.begin(), questions.begin() + count);
}
void SqliteDatabase::executeQuery(const std::string & query)
{
	_db.createQuery(query);
	_db.runQuery();
}

void SqliteDatabase::addStatistic(const std::string & username, int game_id, int correctAns, int wrongAns, int totalTime, int questionCount)
{
	std::lock_guard<std::mutex> guard(_dbMutex);
	_db.createQuery("INSERT INTO Statistics (username, game_id, correct_ans_count, wrong_ans_count, question_count, total_ans_time)"
		"VALUES (?, ?, ?, ?, ?, ?);") << username << game_id << correctAns << wrongAns
		<< questionCount << totalTime;
	_db.runQuery();
}
std::map<std::string, int> SqliteDatabase::getHighScores()
{
	_db.createQuery("SELECT username, correct_ans_count FROM Statistics ORDER BY correct_ans_count DESC;");

	std::map<std::string, int> scores;
	while (_db.runQuery() && scores.size() < MAX_SCORES)
	{
		std::string name;
		int score;

		_db >> name >> score;
		if (score > 0)
			scores.insert({ name, score });
	}

	if (scores.size() <= MAX_SCORES)
		return scores;
	auto last = scores.begin();
	std::advance(last, MAX_SCORES);
	return std::map(scores.begin(), last);
}
void SqliteDatabase::addUser(const std::string & username, const std::string & password, const std::string & email)
{
	std::lock_guard<std::mutex> guard(_dbMutex);
	_db.createQuery("INSERT INTO User VALUES (?, ?, ?);") << username << password << email;
	_db.runQuery();
}
bool SqliteDatabase::matchPassword(const std::string & username, const std::string & password)
{
	_db.createQuery("SELECT password FROM User WHERE username=?;") << username;
	if (_db.runQuery())
	{
		std::string pass;
		_db >> pass;
		return pass == password;
	}
	return false;	// if user not found
}
PlayerStatistics SqliteDatabase::getPlayerStatistics(const std::string & name)
{
	_db.createQuery("SELECT SUM(correct_ans_count), SUM(wrong_ans_count), 1.0*SUM(total_ans_time) / SUM(question_count), COUNT(game_id)"
		"FROM Statistics WHERE username=?;") << name;

	int correctAnsCount = 0;
	int wrongAnsCount = 0;
	int gameCount = 0;
	double avgTime = 0;

	try
	{
		if (_db.runQuery())
			_db >> correctAnsCount >> wrongAnsCount >> avgTime >> gameCount;
	}
	catch (SqliteTypeException & e)
	{
		if (e.Got() != SQLITE_NULL)		// SUM returns null if no records.
			throw e;
	}

	return PlayerStatistics(correctAnsCount, wrongAnsCount, avgTime, gameCount, name);
}
int SqliteDatabase::startGame()
{
	std::lock_guard<std::mutex> guard(_dbMutex);
	_db.createQuery("INSERT INTO Game(status, start_time) VALUES (0, ?);") << (int)CTime::GetCurrentTime().GetTime();
	_db.runQuery();

	_db.createQuery("SELECT game_id FROM Game ORDER BY game_id DESC LIMIT 1");
	_db.runQuery();
	int gameId = 0;
	_db >> gameId;
	return gameId;
}
void SqliteDatabase::endGame(int gameId)
{
	std::lock_guard<std::mutex> guard(_dbMutex);
	_db.createQuery("UPDATE Game "
		"SET end_time = ?, status = 1 WHERE game_id=?;") << CTime::GetCurrentTime().GetTime() << gameId;
	_db.runQuery();
}
