#pragma once

#include "IDatabase.h"
#include "Sqlite.h"
#include <mutex>


class SqliteDatabase : public IDatabase
{
public:
	SqliteDatabase();
	~SqliteDatabase();

	void addUser(const std::string& username, const std::string& password, const std::string& email);
	bool matchPassword(const std::string& username, const std::string& password);
	void addStatistic(const std::string& username, int game_id, int correctAns, int wrongAns, int totalTime, int questionCount);
	std::map<std::string, int> getHighScores();
	PlayerStatistics getPlayerStatistics(const std::string& name);
	bool doesUserExist(const std::string& username);
	unsigned int getQuestionCount();
	std::vector<Question> getQuestions(unsigned int count);
	int startGame(); // returns the new game id
	void endGame(int gameId);

private:
	Sqlite _db;
	static std::mutex _dbMutex;

	void executeQuery(const std::string& query);
	void initDB();
};
