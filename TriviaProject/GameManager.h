#pragma once

#include "IDatabase.h"
#include "Game.h"
#include "Room.h"
#include <mutex>


class GameManager
{
public:
	GameManager(IDatabase& database);
	void addStatistic(const Game& game, const LoggedUser& user);

	Game* CreateGame(Room* room);
	void DeleteGame(Game* game);

private:
	IDatabase& _database;
	std::vector<Game*> _games;
	static std::mutex _managerMutex;
};
