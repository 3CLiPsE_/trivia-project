#pragma once

#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "IRoomRequestHandler.h"


class RoomMemberHandler : public IRoomRequestHandler
{
public:
	RoomMemberHandler(Room* room, const LoggedUser& user, RoomManager& roomManager, RequestHandlerFactory* handlerFactory);
	~RoomMemberHandler();

	virtual bool isRequestRelevant(const Request& request);
	virtual RequestResult handleRequest(const Request& request);

private:
	RequestHandlerFactory* _handlerFactory;

	RequestResult leaveRoom(const Request& request);
	RequestResult getRoomState(const Request& request);
};
