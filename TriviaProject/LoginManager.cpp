#include "LoginManager.h"
#include "MyException.h"
#include <iostream>
#include <algorithm>


std::mutex LoginManager::_managerMutex;

LoginManager::LoginManager(IDatabase& database) : _database(database)
{

}

void LoginManager::signup(const LoggedUser& user, const std::string& password, const std::string& email)
{
	std::lock_guard<std::mutex> guard(_managerMutex);
	try
	{
		_database.addUser(user.getUsername(), password, email);
	}
	catch (const std::exception & e)
	{
		std::cout << e.what() << std::endl;
		throw MyException("A user with this name already exists");
	}
	_loggedUsers.push_back(user);
}
void LoginManager::login(const LoggedUser& user, const std::string& password)
{
	std::lock_guard<std::mutex> guard(_managerMutex);
	if (std::find_if(_loggedUsers.begin(), _loggedUsers.end(), [user](LoggedUser & loggedUser)
		{
			return user.getUsername() == loggedUser.getUsername();
		}) != _loggedUsers.end())
		throw MyException("User already connected");

		if (!_database.matchPassword(user.getUsername(), password))
		{
			throw MyException("Incorrect Password or Username");
		}
		_loggedUsers.push_back(user);
}
void LoginManager::logout(const std::string& username)
{
	std::lock_guard<std::mutex> guard(_managerMutex);

	_loggedUsers.erase(		// erase-remove idiom
		std::remove_if(_loggedUsers.begin(), _loggedUsers.end(),
			[username](const LoggedUser & user) { return username == user.getUsername(); }),	// if usernames match
		_loggedUsers.end());
}
