#include <iostream>
#include "Server.h"
#include "SqliteDatabase.h"


int main()
{
	SqliteDatabase db;
	Server server(db);

	server.run();

	return 0;
}
