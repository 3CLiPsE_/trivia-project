#pragma once

#include "GameData.h"
#include "LoggedUser.h"
#include <unordered_map>
#include <vector>
#include <mutex>


class Game
{
public:	
	Game(unsigned int id, unsigned int roomId, unsigned int timeForQuestion, std::vector<Question> questions, std::vector<LoggedUser> players);
	~Game();

	Question getCurrentQuestionForUser(const LoggedUser& user);
	Question getQuestionForUser(const LoggedUser& user);
	void submitAnswer(const LoggedUser& user, unsigned int answerId, unsigned int answerTime);
	void removePlayer(const LoggedUser& user, bool answered); //we might change this to userID
	std::unordered_map<LoggedUser, GameData> getPlayers() const;
	GameData getGameDataForUser(const LoggedUser& user) const;
    unsigned int getId() const;
    unsigned int getOriginRoomId() const;

	bool gameEmpty() const;
	bool operator==(const Game& other);
	bool operator!=(const Game& other);

private:
	unsigned int _id;
	unsigned int _originRoomId;
	bool _nextQuestion;

	std::vector<Question> _questions;
	std::unordered_map<LoggedUser, GameData> _players;
	std::vector<LoggedUser> _didntAnswerPlayers;
	std::vector<LoggedUser> _loggedUsers;
	std::mutex _gameMutex;
	std::mutex _didntAnswerMutex;
	std::condition_variable _didntAnswerCv;
	std::mutex _nextQuestionMutex;
	std::condition_variable _nextQuestionCV;

	void manageTimerForGame(unsigned int timeForQuestion);
};
