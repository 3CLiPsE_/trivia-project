#pragma once

#include <vector>
#include <queue>
#include "LoggedUser.h"
#include "RoomData.h"
#include <mutex>


class Room
{
public:
	Room(unsigned int id, const std::string& name, unsigned int maxPlayers, unsigned int timePerQuestion, unsigned int questionCount, bool isActive);
	~Room();

	void addUser(const LoggedUser& user);
	void removeUser(const LoggedUser& user);
	std::vector<LoggedUser> getAllUsers() const;
	RoomData getRoomData() const;
	void startGame();

	bool operator==(const Room& other) const;

private:
	RoomData _metadata;
	std::vector<LoggedUser> _users;
	std::mutex _roomMutex;
};
