#pragma once

#include <vector>
#include <string>
#include <map>
#include "RoomData.h"
#include "LoggedUser.h"
#include "Room.h"
#include "PlayerStatistics.h"


struct LogoutResponse
{
	unsigned int _status;

	LogoutResponse(unsigned int status);
};

struct GetRoomsRsponse
{
	unsigned int _status;
	std::vector<RoomData> _rooms;

	GetRoomsRsponse(unsigned int status, const std::vector<Room*>& rooms);
};

struct GetPlayersInRoomResponse
{
	unsigned int _status;
	std::vector<std::string> _players;
	
	GetPlayersInRoomResponse(unsigned int status, const std::vector<LoggedUser>& players);
};

struct HighscoreResponse
{
	unsigned int _status;
	std::map<std::string, int> _highscores;

	HighscoreResponse(unsigned int status, const std::map<std::string, int>& highscores);
};

struct StatisticResponse
{
	unsigned int _status;
	int _correctAnsCount;
	int _wrongAnsCount;
	double _avgAnsTime;

	StatisticResponse(unsigned int _status, const PlayerStatistics& statistics);
};

struct JoinRoomResponse
{
	unsigned int _status;

	JoinRoomResponse(unsigned int status);
};

struct CreateRoomResponse
{
	unsigned int _status;
	unsigned int _roomId;

	CreateRoomResponse(unsigned int status, unsigned int roomId);
};
