#include "GameData.h"
#include <numeric>


GameData::GameData(unsigned int questionIndex)
	: GameData(questionIndex, 0, 0, 0)
{

}
GameData::GameData(unsigned int questionIndex, unsigned int correctAnswerCount, unsigned int wrongAnswerCount, unsigned int averageAnswerTime) :
	_questionIndex(questionIndex), _correctAnswerCount(correctAnswerCount), _wrongAnswerCount(wrongAnswerCount), _averageAnswerTime(averageAnswerTime)
{

}

void GameData::addAnswer(bool isCorrect, unsigned int answerTime)
{
	if (isCorrect)
		_correctAnswerCount++;
	else
		_wrongAnswerCount++;
	_answerTimes.push_back(answerTime);
	_averageAnswerTime = std::accumulate(_answerTimes.begin(), _answerTimes.end(), 0) / _answerTimes.size();
}
