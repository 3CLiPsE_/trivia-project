#pragma comment(lib, "ws2_32.lib") 

#include "Communicator.h"
#include "IRequestHandler.h"
#include "Helper.h"
#include "SocketExceptions.h"
#include "MyException.h"
#include "SqliteException.h"
#include <thread>
#include <algorithm>
#include <atltime.h>
#include <iostream>

#define CONNECTION_CLOSED 0
#define MAX_BUFFER_LEN 2048


std::mutex Communicator::_messageMutex;
std::shared_mutex Communicator::_idsMutex;
std::shared_mutex Communicator::_clientsMutex;
std::queue<std::pair<RequestResult, SOCKET>> Communicator::_messageQueue;
std::map<unsigned int, SOCKET> Communicator::_idSocketMap;
std::condition_variable Communicator::_messageQueueCond;
std::map<SOCKET, IRequestHandler*> Communicator::_clients;

Communicator::Communicator(RequestHandlerFactory* handlerFactory) :
	_handlerFactory(handlerFactory), _nextClientId(0)
{
	_serverSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
}
Communicator::~Communicator()
{
	closesocket(_serverSock);
}

void Communicator::startThreadForNewClient(SOCKET client)
{
	try
	{
		while (true)
		{
			Request request = getRequest(client);

			RequestResult result(REQUEST_UNSUCCESSFUL, std::vector<char>(), nullptr);

			std::shared_lock<std::shared_mutex> lock(_clientsMutex);
			if (!_clients[client]->isRequestRelevant(request))
			{
				result = Helper::getErrorResult(request._requestId, "Wrong Request");
			}
			else
			{
				result = _clients[client]->handleRequest(request);

				if (request._requestId == SIGNOUT_REQUEST_CODE) //close the connection with client
					throw SocketDisconnectedException();
				if (result._newHandler != nullptr)
				{
					delete _clients[client];
					_clients[client] = result._newHandler;
				}
			}

			send(client, result._response.data(), (int)result._response.size(), 0);
		}
	}
	catch (SocketDisconnectedException&)
	{
		std::unique_lock<std::shared_mutex> guard(_clientsMutex);

		IRequestHandler* currentHandler = _clients[client];

		while (currentHandler != nullptr)
		{
			//if the client discconected inside a room than handle him like he sent a LeaveRoom request
			if (currentHandler->isRequestRelevant(Request(LEAVE_ROOM_REQUEST_CODE, std::vector<char>())))
			{
				RequestResult res = currentHandler->handleRequest(Request(LEAVE_ROOM_REQUEST_CODE, std::vector<char>()));
				delete currentHandler;
				currentHandler = res._newHandler;
			}

			//if the client discconected while being the admin of a room than handle him like he sent a CloseRoom request
			else if (currentHandler->isRequestRelevant(Request(CLOSE_ROOM_REQUEST_CODE, std::vector<char>())))
			{
				RequestResult res = currentHandler->handleRequest(Request(CLOSE_ROOM_REQUEST_CODE, std::vector<char>()));
				delete currentHandler;
				currentHandler = res._newHandler;
			}

			//if the client discconected during a game
			else if (currentHandler->isRequestRelevant(Request(LEAVE_GAME_REQUEST_CODE, std::vector<char>())))
			{
				RequestResult res = currentHandler->handleRequest(Request(LEAVE_GAME_REQUEST_CODE, std::vector<char>()));
				delete currentHandler;
				currentHandler = res._newHandler;
			}

			//if the client discconected in the menu
			else if (currentHandler->isRequestRelevant(Request(SIGNOUT_REQUEST_CODE, std::vector<char>())))
			{
				RequestResult res = currentHandler->handleRequest(Request(SIGNOUT_REQUEST_CODE, std::vector<char>()));
				delete currentHandler;
				currentHandler = res._newHandler;
			}

			//if the user hasn't log in yet 
			else
			{
				delete currentHandler;
				currentHandler = nullptr;
			}
		}
		std::unique_lock<std::shared_mutex> idsGuard(_idsMutex);
		_clients.erase(client);
		_idSocketMap.erase(client);
		closesocket(client);
		return;
	}
	catch (std::exception & e)
	{
		TRACE(e.what());
		std::cout << std::endl;
	}
	catch (...)
	{
		TRACE("Something happened\n");
	}
}
void Communicator::bindAndListen()
{
	sockaddr_in sa = { 0 };
	sa.sin_addr.s_addr = INADDR_ANY;
	sa.sin_family = AF_INET;
	sa.sin_port = htons(PORT);

	if (bind(_serverSock, (sockaddr*)& sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception("A problem accoured while binding the server's socket");
	if (listen(_serverSock, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception("A problem accoured when started listening in the server's socket");
}
void Communicator::handleRequests()
{
	std::thread messageThread(&Communicator::startThreadForNotifications, this); // start thread for push notifications
	messageThread.detach();
	while (true)
	{
		try
		{
			SOCKET clientSock = accept(_serverSock, NULL, NULL);
			if (clientSock == INVALID_SOCKET)
				throw std::exception("Error while accepting client");

			std::unique_lock<std::shared_mutex> clientsGuard(_clientsMutex);
			_clients.insert({ clientSock, _handlerFactory->createLoginRequestHandler(_nextClientId) });
			std::unique_lock<std::shared_mutex> idsGuard(_idsMutex);
			_idSocketMap.insert({ _nextClientId, clientSock });
			_nextClientId++;

			std::thread t(&Communicator::startThreadForNewClient, this, clientSock);
			t.detach();		// client runs until disconnected
		}
		catch (const std::exception & e)
		{
			TRACE((std::string(e.what()) + '\n').c_str());
		}
	}
}
Request Communicator::getRequest(SOCKET client)
{
	CTime recvTime = CTime::GetCurrentTime();
	BYTE requestCode;
	if (recv(client, (char*)(&requestCode), sizeof(char), MSG_WAITALL) == CONNECTION_CLOSED)
		throw SocketDisconnectedException();
	u_long len;
	if (recv(client, (char*)& len, sizeof(int), MSG_WAITALL) == CONNECTION_CLOSED)
		throw SocketDisconnectedException();
	len = ntohl(len); //convert from network byte order to host byte order (eg. little endian)

	if (len > MAX_BUFFER_LEN)
		throw SocketDisconnectedException();

	std::vector<char> content((size_t)len);
	if (len != 0)
	{
		if (recv(client, content.data(), (int)len, MSG_WAITALL) == CONNECTION_CLOSED)
			throw SocketDisconnectedException();
	}

	return Request(requestCode, recvTime, content);
}

void Communicator::startThreadForNotifications()
{
	while (true)
	{
		std::unique_lock<std::mutex> lock(_messageMutex);
		_messageQueueCond.wait(lock, [] {return !_messageQueue.empty(); });

		std::pair<RequestResult, SOCKET> message = _messageQueue.front();
		_messageQueue.pop();
		lock.unlock();

		std::unique_lock<std::shared_mutex> guard(_clientsMutex);

		RequestResult result = message.first;
		send(message.second, result._response.data(), (int)result._response.size(), 0);

		//update client handler
		if (message.first._newHandler != nullptr)
		{
			delete _clients[message.second];
			_clients[message.second] = message.first._newHandler;
		}
	}
}
void Communicator::addRequestToQueue(RequestResult result, int userId)
{
	std::lock_guard<std::mutex> lock(Communicator::_messageMutex);

	std::shared_lock<std::shared_mutex> idsLock(_idsMutex);
	_messageQueue.push({ result, Communicator::_idSocketMap[userId] });

	_messageQueueCond.notify_all();
}
