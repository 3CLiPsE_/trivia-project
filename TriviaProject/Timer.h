#pragma once
#include <chrono>
#include <thread>

template <class T>
[event_source(native)]
class Timer
{
	std::chrono::seconds _time;
	void (T::*_handler)();
	T* _sourceObject;
	bool _stop;

	__event void tickEvent();

public:
	template <class T>
	Timer(std::chrono::seconds time, void (T::* handler)(), T* sourceObject) :
		_time(time), _handler(handler), _sourceObject(sourceObject), _stop(false)
	{
		__hook(_handler, _sourceObject);
	}

	template <class T>
	~Timer()
	{
		__unhook(_handler, _sourceObject);
	}
	
	template <class T>
	void start()
	{
		_stop = false;
		auto startTime = std::chrono::system_time::now();
		while (!_stop && std::chrono::system_time::now() - startTime < _time)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(10)); //in order to not overload the cpu
		}

		__raise tickEvent;
	}

	void stop()
	{
		_stop = true;
	}
};

