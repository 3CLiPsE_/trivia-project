#include "SqliteException.h"


SqliteException::SqliteException(const std::string& errorMsg) : _msg("Sqlite error happenned: ")
{
	_msg.append(errorMsg);
}
const char* SqliteException::what() const noexcept
{
	return _msg.c_str();
}

SqliteTypeException::SqliteTypeException(const std::string& errorMsg, int expected, int got) :
	SqliteException(errorMsg)
{
	_expected = expected;
	_got = got;
}
const char* SqliteTypeException::what() const noexcept
{
	return _msg.c_str();
}
int SqliteTypeException::Expected() const
{
	return _expected;
}
int SqliteTypeException::Got() const
{
	return _got;
}
