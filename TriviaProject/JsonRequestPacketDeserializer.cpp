#include "JsonRequestPacketDeserializer.h"
#include "json.hpp"

#define USERNAME "Username" 
#define PASSWORD "Password"
#define EMAIL_ADDRESS "Email"
#define ROOM_ID "Room_id"
#define ROOM_NAME "Name"
#define MAX_USERS "Max_users"
#define QUESTION_COUNT "Questions_count"
#define QUESTION_TIME "Time"
#define ANSWER_ID "Answer_Id"


LoginRequest  JsonRequestPacketDeserializer::deserializeLoginRequest(const std::vector<char>& buffer)
{
	nlohmann::json jsonRequest = nlohmann::json::from_msgpack(buffer);

	return LoginRequest(jsonRequest[USERNAME], jsonRequest[PASSWORD]);
}
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(const std::vector<char>& buffer)
{
	nlohmann::json jsonRequest = nlohmann::json::from_msgpack(buffer);

	return SignupRequest(jsonRequest[USERNAME], jsonRequest[PASSWORD], jsonRequest[EMAIL_ADDRESS]);
}
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(const std::vector<char>& buffer)
{
	nlohmann::json jsonRequest = nlohmann::json::from_msgpack(buffer);

	return GetPlayersInRoomRequest(jsonRequest[ROOM_ID]);
}
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(const std::vector<char>& buffer)
{
	nlohmann::json jsonRequest = nlohmann::json::from_msgpack(buffer);

	return JoinRoomRequest(jsonRequest[ROOM_ID]);
}
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(const std::vector<char>& buffer)
{
	nlohmann::json jsonRequest = nlohmann::json::from_msgpack(buffer);

	return CreateRoomRequest(jsonRequest[ROOM_NAME], jsonRequest[MAX_USERS], jsonRequest[QUESTION_COUNT], jsonRequest[QUESTION_TIME]);
}
SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSumbitAnswerRequest(const std::vector<char>& buffer)
{
	nlohmann::json jsonRequest = nlohmann::json::from_msgpack(buffer);

	return SubmitAnswerRequest(jsonRequest[ANSWER_ID]);
}
