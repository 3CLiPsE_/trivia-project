#include "JsonResponsePacketSerializer.h"
#include "json.hpp"
#include "Helper.h"

#define STATUS "Status"
#define MESSAGE "Message"
#define ROOMS "Rooms"
#define PLAYERS "Players"
#define ROOM_NAME "Name"
#define ROOM_ID "Id"
#define ROOM_TIME "Question_time"
#define ROOM_ACTIVE "Active"
#define ROOM_PLAYERS "Max_players"
#define ROOM_QUESTIONS "Question_num"
#define HIGHSCORES "Highscores"
#define RESULTS "Results"
#define USERNAME "Username"
#define CORRECT_COUNT "Correct"
#define WRONG_COUNT "Wrong"
#define AVG_TIME "Avg_time"
#define CORRECT_ANSWER_INDEX "Correct_answer_index"
#define ANSWERS "Answers"
#define QUESTION "Question"


std::vector<char> JsonResponsePacketSerializer::serializeResponse(const ErrorResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[MESSAGE] = response._msg;
	jsonResponse[STATUS] = REQUEST_UNSUCCESSFUL;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const LoginResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const SignupResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const LogoutResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const GetRoomsRsponse& response)
{
	nlohmann::json jsonResponse = nlohmann::json::object();

	jsonResponse[STATUS] = response._status;
	jsonResponse[ROOMS] = nlohmann::json::array();

	for (RoomData room : response._rooms)
	{
		nlohmann::json roomData = nlohmann::json::object();

		roomData[ROOM_ID] = room._id;
		roomData[ROOM_NAME] = room._name;
		roomData[ROOM_ACTIVE] = room._isActive;
		roomData[ROOM_TIME] = room._timePerQuestion;
		roomData[ROOM_PLAYERS] = room._maxPlayers;
		roomData[ROOM_QUESTIONS] = room._questionCount;

		jsonResponse[ROOMS].push_back(roomData);
	}

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const GetPlayersInRoomResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;
	jsonResponse[PLAYERS] = response._players;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const JoinRoomResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const CreateRoomResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;
	jsonResponse[ROOM_ID] = response._roomId;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const HighscoreResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;
	jsonResponse[HIGHSCORES] = response._highscores;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const StatisticResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;
	jsonResponse[CORRECT_COUNT] = response._correctAnsCount;
	jsonResponse[WRONG_COUNT] = response._wrongAnsCount;
	jsonResponse[AVG_TIME] = response._avgAnsTime;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}


std::vector<char> JsonResponsePacketSerializer::serializeResponse(const CloseRoomResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const GetRoomStateResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;
	jsonResponse[ROOM_TIME] = response._answerTimeout;
	jsonResponse[ROOM_ACTIVE] = response._hasGameBegun;
	jsonResponse[ROOM_QUESTIONS] = response._questionCount;
	jsonResponse[PLAYERS] = nlohmann::json::array();

	for (auto player : response._players)
	{
		jsonResponse[PLAYERS].push_back(player);
	}

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const LeaveRoomResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const StartRoomResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}

std::vector<char> JsonResponsePacketSerializer::serializeResponse(const GetGameResultResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;
	jsonResponse[RESULTS] = nlohmann::json::array();

	for (auto& res : response._results)
	{
		nlohmann::json result = nlohmann::json::object();

		result[USERNAME] = res._username;
		result[CORRECT_COUNT] = res._correctAnswerCount;
		result[WRONG_COUNT] = res._wrongAnswerCount;
		result[AVG_TIME] = res._averageAnswerTime;

		jsonResponse[RESULTS].push_back(result);
	}

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const SubmitAnswerResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;

	jsonResponse[CORRECT_ANSWER_INDEX] = response._correctAnswer;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
std::vector<char> JsonResponsePacketSerializer::serializeResponse(const GetQuestionResponse& response)
{
	nlohmann::json jsonResponse;

	jsonResponse[STATUS] = response._status;
	jsonResponse[QUESTION] = response._question;
	jsonResponse[ANSWERS] = response._answers;

	return Helper::toCharArray(nlohmann::json::to_msgpack(jsonResponse));
}
