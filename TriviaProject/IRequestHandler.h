#pragma once

#include "Request.h"
#include "RequestResult.h"


struct RequestResult;

class IRequestHandler
{
public:
	virtual bool isRequestRelevant(const Request& request) = 0;
	virtual RequestResult handleRequest(const Request& request) = 0;
};
