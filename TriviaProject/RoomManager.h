#pragma once

#include "Room.h"
#include "IDatabase.h"
#include <map>
#include <string>
#include <vector>
#include <string>
#include <queue>
#include <mutex>


class RoomManager
{
public:
	RoomManager(IDatabase& db);
	int CreateRoom(const LoggedUser& user, const std::string& name, unsigned int timePerQuestion, unsigned int questionCount, unsigned int maxPlayers);
	void DeleteRoom(int id);
	void RemoveUserFromRoom(const LoggedUser& user);
	void AddUserToRoom(int id, const LoggedUser& user);
	unsigned int getRoomState(int id);
	std::vector<Room*> getRooms();
	Room* getRoomById(int id);

private:
	IDatabase& _db;
	std::map<int, Room*> _rooms;
	static std::mutex _managerMutex;
	int _next_id;
};
