#include "Question.h"
#include <algorithm>
#include <random>


Question::Question(const std::string& question, const std::string& correct, const std::vector<std::string>& answers) :
	_question(question), _answers(answers)
{
	_answers.push_back(correct);
	auto rng = std::default_random_engine();
	std::shuffle(_answers.begin(), _answers.end(), rng);

	_correct = std::find(_answers.begin(), _answers.end(), correct) - _answers.begin();
}
Question::Question(const Question& question) :
	_question(question.getQuestion()), _answers(question.getPossibleAnswers())
{
	_correct = question.getCorrectAnswerId();
}
Question::~Question()
{
}

std::vector<std::string> Question::getPossibleAnswers() const
{
	return _answers;
}
std::string Question::getQuestion() const
{
	return _question;
}
int Question::getCorrectAnswerId() const
{
	return _correct;
}
