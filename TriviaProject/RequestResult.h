#pragma once

#include "IRequestHandler.h"
#include <vector>


class IRequestHandler;

struct RequestResult
{
	std::vector<char> _response;
	IRequestHandler* _newHandler;

	RequestResult(BYTE code, const std::vector<char>& buffer, IRequestHandler* requestHandler);
	RequestResult();
};
