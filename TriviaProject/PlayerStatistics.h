#pragma once

#include <string>


struct PlayerStatistics
{
	int _correctAnsCount;
	int _wrongAnsCount;
	double _avgAnsTime;
	int _numberOfGames;
	std::string _name;

	PlayerStatistics(int correctAnsCount, int wrongAnsCount, double avgAnsCount, int numberOfGames, const std::string& name);
};
