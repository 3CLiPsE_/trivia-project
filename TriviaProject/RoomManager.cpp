#include "RoomManager.h"
#include "Communicator.h"
#include "MyException.h"
#include <algorithm>


std::mutex RoomManager::_managerMutex;

RoomManager::RoomManager(IDatabase& db) :
	_db(db), _next_id(1)
{

}
int RoomManager::CreateRoom(const LoggedUser& user, const std::string& name, unsigned int timePerQuestion, unsigned int questionCount, unsigned int maxPlayers)
{
	std::lock_guard<std::mutex> guard(_managerMutex);

	auto compareRooms = [name](const std::pair<int, Room*> & room)
	{
		return room.second->getRoomData()._name == name;
	};
	if (std::find_if(_rooms.begin(), _rooms.end(), compareRooms) != _rooms.end())
		throw MyException("Room already exists");

	if (questionCount <= 0)
		throw MyException("Question Count Must Be Positive");
	if (_db.getQuestionCount() < questionCount)
		throw MyException("Not enough questions in database.");
	unsigned id = _next_id++;
	_rooms.insert({ id, new Room(id, name, maxPlayers, timePerQuestion, questionCount, false) });
	_rooms.at(id)->addUser(user);
	return id;
}
void RoomManager::DeleteRoom(int id)
{
	std::lock_guard<std::mutex> guard(_managerMutex);

	delete _rooms.at(id);
	_rooms.erase(id);
}
unsigned int RoomManager::getRoomState(int id)
{
	std::lock_guard<std::mutex> guard(_managerMutex);

	return _rooms.at(id)->getRoomData()._isActive;
}
std::vector<Room*> RoomManager::getRooms()
{
	std::lock_guard<std::mutex> guard(_managerMutex);

	std::vector<Room*> rooms;
	for (auto const& r : _rooms)
		rooms.push_back(r.second);
	return rooms;
}
void RoomManager::RemoveUserFromRoom(const LoggedUser & user)
{
	std::lock_guard<std::mutex> guard(_managerMutex);

	std::map<int, Room*>::iterator targetRoom = std::find_if(_rooms.begin(), _rooms.end(), [user](const std::pair<int, Room*> & room)
		{
			std::vector<LoggedUser> players = room.second->getAllUsers();
			return std::find(players.begin(), players.end(), user) != players.end();
		});

	if (targetRoom != _rooms.end())
		targetRoom->second->removeUser(user);
}
void RoomManager::AddUserToRoom(int id, const LoggedUser & user)
{
	std::lock_guard<std::mutex> guard(_managerMutex);

	if (_rooms.find(id) == _rooms.end())
		throw std::exception("Invalid Id");
	if (_rooms.at(id)->getRoomData()._isActive)
		throw std::exception("Room Active");
	_rooms.at(id)->addUser(user);
}
Room* RoomManager::getRoomById(int id)
{
	std::lock_guard<std::mutex> guard(_managerMutex);

	return _rooms.at(id);
}
