import tests
import inspect
import time
import os
from constants import Colors


RUN_ALL_OPTION = '1'
DEBUG_TEST = '2'
DEBUG_ALL = '3'
COLORS = Colors()


def test(test_func, debug):
    if debug:
        print(f"Starting to debug {COLORS.NAME.format(test_func[0])}")
    try:
        test_func[1](debug)
    except AssertionError as e:
        print(f"{COLORS.NAME.format(test_func[0])} {COLORS.FAILED}\nerror: {e}")
    except Exception as e:
        print(f"{COLORS.NAME.format(test_func[0])} {COLORS.FAILED}\nAn unexpected error has occurred:"
              f" {type(e).__name__} - {e}")
    else:
        print(f"{COLORS.NAME.format(test_func[0])} {COLORS.PASSED}")


def main():
    # get tests
    test_funcs = [func for func in inspect.getmembers(tests.Tests, inspect.isroutine) if not func[0].startswith('_')]

    tests.Tests.db_path = input("Enter server database path: ")

    while True:
        print("""1. Run All Tests
2. Debug Specific Test
3. Debug All Tests
Enter something else to exit""")

        user_choice = input("Your Choice: ")

        if user_choice == RUN_ALL_OPTION:
            for test_func in test_funcs:
                test(test_func, False)
                time.sleep(0.5)
        elif user_choice == DEBUG_TEST:
            [print(f"{i}. {COLORS.NAME.format(test_funcs[i][0])}") for i in range(len(test_funcs))]
            user_choice = int(input("Your Choice:"))
            test(test_funcs[user_choice], True)
        elif user_choice == DEBUG_ALL:
            for test_func in test_funcs:
                test(test_func, True)
                os.system('pause')
        else:
            break
        os.system('pause')
        os.system('cls')


if __name__ == '__main__':
    main()
