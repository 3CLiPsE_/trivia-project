import sys
import socket
from constants import *
import sqlite3
import time
import os
try:
    import msgpack
except ImportError:
    print("Please install msgpack module")
    sys.exit(-1)

SERVER_ADDR = ('127.0.0.1', 1337)
NETWORK_BYTE_ORDER = 'big'
INT_SIZE = 4
BYTE_SIZE = 1
COLORS = Colors()


class Tests:
    db_path = ""

    @staticmethod
    def login_test_correct(debug):
        if debug:
            Helper.print_debug("creating client (username=a, password=123)")
        c = Client(debug)
        ans = c.login('a', '123')
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])

    @staticmethod
    def login_test_wrong_name(debug):
        if debug:
            Helper.print_debug("creating client (username=asdsad, password=123)")
        c = Client(debug)
        ans = c.login('asdsad', '123')
        Helper.assert_result(REQUEST_UNSUCCESSFUL, ans[ResponseJson.STATUS])

    @staticmethod
    def login_test_wrong_password(debug):
        if debug:
            Helper.print_debug("creating client (username=a, password=abcd)")
        c = Client(debug)
        ans = c.login('a', 'abcd')
        Helper.assert_result(REQUEST_UNSUCCESSFUL, ans[ResponseJson.STATUS])

    @staticmethod
    def login_test_duplicate(debug):
        first_user = Helper.create_client('a', '123', debug)
        if debug:
            Helper.print_debug("creating second client (username=a, password=123)")
        second_user = Client(debug)
        ans = second_user.login('a', '123')
        Helper.assert_result(REQUEST_UNSUCCESSFUL, ans[ResponseJson.STATUS])

    @staticmethod
    def get_rooms_test_empty(debug):
        c = Helper.create_client('a', '123', debug)
        ans = c.get_rooms()
        Helper.assert_result([], ans[ResponseJson.ROOMS])

    @staticmethod
    def create_room_test(debug):
        c = Helper.create_client('a', '123', debug)
        if debug:
            Helper.print_debug("creating room (name=test, time=3, questions=2, players=2")
        ans = c.create('test', 3, 2, 2)
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])

    @staticmethod
    def duplicate_room_test(debug):
        first_admin = Helper.create_client('a', '123', debug)
        Helper.create_room(first_admin, 'a', 1, 2, 3, debug)
        second_admin = Helper.create_client('b', '123', debug)
        ans = second_admin.create('a', 1, 2, 3)
        Helper.assert_result(REQUEST_UNSUCCESSFUL, ans[ResponseJson.STATUS])

    @staticmethod
    def join_room_test(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'get_players_test', 3, 2, 3, debug)
        member = Helper.create_client('c', '123', debug)
        target_room_id = Helper.get_room_id(member, debug)
        ans = member.join(target_room_id)
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])
        admin.recv_msg(ProtocolCodes.GET_ROOM_STATE_REQUEST_CODE)
        admin.close_room()
        member.recv_msg(ProtocolCodes.CLOSE_ROOM_REQUEST_CODE)

    @staticmethod
    def join_active_room_test(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'get_players_test', 3, 2, 3, debug)
        admin.start_game()
        member = Helper.create_client('c', '123', debug)
        target_room_id = Helper.get_room_id(member, debug)
        ans = member.join(target_room_id)
        Helper.assert_result(REQUEST_UNSUCCESSFUL, ans[ResponseJson.STATUS])

    @staticmethod
    def get_rooms_test_not_empty(debug):
        admin = Helper.create_client('a', '123', debug)
        if debug:
            Helper.print_debug("creating room (name=test, time=3, questions=2, players=2")
        ans = admin.create('test', 3, 2, 2)
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])
        c = Helper.create_client('b', '123', debug)
        ans = c.get_rooms()
        if debug:
            Helper.print_debug(ans[ResponseJson.ROOMS])
        Helper.assert_condition(ans[ResponseJson.ROOMS],
                                lambda v: len(v) == 1 and v[0][ResponseJson.ROOM_NAME] == 'test')

    @staticmethod
    def get_players_test_only_admin(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'get_players_test', 3, 2, 3, debug)
        c = Helper.create_client('b', '123', debug)
        target_room_id = Helper.get_room_id(c, debug)
        ans = c.get_players(target_room_id)
        Helper.assert_result(['a'], ans[ResponseJson.PLAYERS])

    @staticmethod
    def get_players_test_with_members(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'get_players_test', 3, 2, 3, debug)
        member = Helper.create_client('c', '123', debug)
        target_room_id = Helper.get_room_id(member, debug)
        member.join(target_room_id)
        admin.recv_msg(ProtocolCodes.GET_ROOM_STATE_REQUEST_CODE)
        c = Helper.create_client('b', '123', debug)
        ans = c.get_players(target_room_id)
        Helper.assert_result(['a', 'c'], ans[ResponseJson.PLAYERS])
        admin.close_room()

    @staticmethod
    def start_game_test(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'start game', 3, 3, 3, debug)
        member = Helper.create_client('b', '123', debug)
        if debug:
            Helper.print_debug("Joining Room")
        ans = member.join(Helper.get_room_id(member, debug))
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])
        if debug:
            Helper.print_debug("member is waiting for game to start")
            Helper.print_debug("starting game")
        admin.recv_msg(ProtocolCodes.GET_ROOM_STATE_REQUEST_CODE)
        ans = admin.start_game()
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])
        member.recv_msg(ProtocolCodes.START_GAME_REQUEST_CODE)

    @staticmethod
    def submit_answer_test(debug):
        assert os.path.exists(Tests.db_path), "invalid database path"

        conn = sqlite3.connect(Tests.db_path)
        cursor = conn.cursor()
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'submit answer', 3, 3, 3, debug)
        if debug:
            Helper.print_debug("starting game")
        admin.start_game()
        ans = admin.get_question()
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])
        if debug:
            Helper.print_debug(f"got question: ({ans[ResponseJson.QUESTION], ans[ResponseJson.ANSWERS]})")
        cursor.execute(r'SELECT correct_ans FROM Question WHERE question = ?', (ans[ResponseJson.QUESTION], ))
        correct_ans_index = ans[ResponseJson.ANSWERS].index(cursor.fetchone()[0])
        ans = admin.submit_answer(correct_ans_index)
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])

        Helper.assert_result(correct_ans_index, ans[ResponseJson.CORRECT_ANSWER_INDEX])

    @staticmethod
    def submit_nonexisting_answer(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'submit_wrong_answer', 3, 3, 3, debug)
        if debug:
            Helper.print_debug("Starting game")
        admin.start_game()
        ans = admin.get_question()
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])
        if debug:
            Helper.print_debug(f"Got question: ({ans[ResponseJson.QUESTION], ans[ResponseJson.ANSWERS]})")
        ans = admin.submit_answer(-1)
        Helper.assert_result(REQUEST_UNSUCCESSFUL, ans[ResponseJson.STATUS])
        if debug:
            Helper.print_debug(f"Server sent: {ans[ResponseJson.MESSAGE]}")

    @staticmethod
    def get_question_test(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'submit answer', 3, 3, 3, debug)
        if debug:
            Helper.print_debug("starting game")
        admin.start_game()
        ans = admin.get_question()
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])
        if debug:
            Helper.print_debug(f"got question: ({ans[ResponseJson.QUESTION], ans[ResponseJson.ANSWERS]})")

    @staticmethod
    def get_questions_test(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'submit answer', 10, 10, 10, debug)
        if debug:
            Helper.print_debug("starting game")
        admin.start_game()
        ans = admin.get_question()
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])
        if debug:
            Helper.print_debug(f"got question: ({ans[ResponseJson.QUESTION], ans[ResponseJson.ANSWERS]})")
        first_question = ans[ResponseJson.QUESTION]
        admin.submit_answer(0)
        ans = admin.get_question()
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])
        Helper.assert_condition((first_question, ans[ResponseJson.QUESTION]), lambda q: q[0] != q[1])

    @staticmethod
    def not_enough_question_test(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'submit answer', 1, 1, 1, debug)
        if debug:
            Helper.print_debug("starting game")
        admin.start_game()
        ans = admin.get_question()
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])
        if debug:
            Helper.print_debug(f"got question: ({ans[ResponseJson.QUESTION], ans[ResponseJson.ANSWERS]})")
        admin.submit_answer(0)
        ans = admin.get_question()
        Helper.assert_result(REQUEST_UNSUCCESSFUL, ans[ResponseJson.STATUS])

    @staticmethod
    def answer_before_question_test(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'a', 3, 3, 4, debug)
        ans = admin.start_game()
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])
        ans = admin.submit_answer(0)
        Helper.assert_result(REQUEST_UNSUCCESSFUL, ans[ResponseJson.STATUS])

    @staticmethod
    def room_deleted_ordinary_test(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'test', 3, 3, 3, debug)
        admin.close_room()
        ans = admin.get_rooms()
        Helper.assert_result([], ans[ResponseJson.ROOMS])

    @staticmethod
    def room_deleted_spontaneously_test(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'test', 3, 3, 3, debug)
        del admin
        tester = Helper.create_client('b', '123', debug)
        ans = tester.get_rooms()
        Helper.assert_result([], ans[ResponseJson.ROOMS])

    @staticmethod
    def exit_app_in_room_test(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'exit test', 2, 3, 4, debug)
        del admin
        another_client = Helper.create_client('b', '123', debug)  # check if the server is still running
        del another_client

    @staticmethod
    def time_out_test(debug):
        admin = Helper.create_client('a', '123', debug)
        Helper.create_room(admin, 'submit answer', 1, 1, 1, debug)
        if debug:
            Helper.print_debug("starting game")
        admin.start_game()
        ans = admin.get_question()
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])
        if debug:
            Helper.print_debug("waiting for 1 second")
        time.sleep(1)
        admin.recv_msg(ProtocolCodes.TIME_OUT_RESPONSE_CODE)


class Client:
    def __init__(self, debug):
        self.client_sock = socket.socket()
        self.client_sock.connect(SERVER_ADDR)
        self._debug = debug

    def login(self, username, password):
        buffer = {RequestJson.USERNAME: username, RequestJson.PASSWORD: password}
        self.__send_msg(Helper.build_msg(ProtocolCodes.LOGIN_REQUEST_CODE, self._debug, buffer))
        return self.recv_msg(ProtocolCodes.LOGIN_REQUEST_CODE)

    def signup(self, username, password, email):
        buffer = {RequestJson.USERNAME: username, RequestJson.PASSWORD: password, RequestJson.EMAIL_ADDRESS: email}
        self.__send_msg(Helper.build_msg(ProtocolCodes.SIGNUP_REQUEST_CODE, self._debug, buffer))
        return self.recv_msg(ProtocolCodes.SIGNUP_REQUEST_CODE)

    def signout(self):
        self.__send_msg(Helper.build_msg(ProtocolCodes.SIGNOUT_REQUEST_CODE, self._debug))
        return self.recv_msg(ProtocolCodes.SIGNOUT_REQUEST_CODE)

    def get_rooms(self):
        self.__send_msg(Helper.build_msg(ProtocolCodes.GET_ROOMS_REQUEST_CODE, self._debug))
        return self.recv_msg(ProtocolCodes.GET_ROOMS_REQUEST_CODE)

    def get_players(self, room_id):
        self.__send_msg(Helper.build_msg(ProtocolCodes.GET_PLAYERS_REQUEST_CODE, self._debug,
                                         {RequestJson.ROOM_ID: room_id}))
        return self.recv_msg(ProtocolCodes.GET_PLAYERS_REQUEST_CODE)

    def get_highscores(self):
        self.__send_msg(Helper.build_msg(ProtocolCodes.GET_HIGHSCORES_REQUEST_CODE, self._debug))
        self.recv_msg(ProtocolCodes.GET_HIGHSCORES_REQUEST_CODE)

    def join(self, room_id):
        self.__send_msg(Helper.build_msg(ProtocolCodes.JOIN_REQUEST_CODE, self._debug, {RequestJson.ROOM_ID: room_id}))
        return self.recv_msg(ProtocolCodes.JOIN_REQUEST_CODE)

    def create(self, name, question_time, count, max_users):
        buffer_dict = {RequestJson.ROOM_NAME: name, RequestJson.QUESTION_TIME: question_time,
                       RequestJson.QUESTION_COUNT: count, RequestJson.MAX_USERS: max_users}

        self.__send_msg(Helper.build_msg(ProtocolCodes.CREATE_REQUEST_CODE, self._debug, buffer_dict))
        return self.recv_msg(ProtocolCodes.CREATE_REQUEST_CODE)

    def close_room(self):
        self.__send_msg(Helper.build_msg(ProtocolCodes.CLOSE_ROOM_REQUEST_CODE, self._debug))
        return self.recv_msg(ProtocolCodes.CLOSE_ROOM_REQUEST_CODE)

    def start_game(self):
        self.__send_msg(Helper.build_msg(ProtocolCodes.START_GAME_REQUEST_CODE, self._debug))
        return self.recv_msg(ProtocolCodes.START_GAME_REQUEST_CODE)

    def get_room_state(self):
        self.__send_msg(Helper.build_msg(ProtocolCodes.GET_ROOM_STATE_REQUEST_CODE, self._debug))
        return self.recv_msg(ProtocolCodes.GET_ROOM_STATE_REQUEST_CODE)

    def leave_room(self):
        self.__send_msg(Helper.build_msg(ProtocolCodes.LEAVE_ROOM_REQUEST_CODE, self._debug))
        return self.recv_msg(ProtocolCodes.LEAVE_ROOM_REQUEST_CODE)

    def get_question(self):
        self.__send_msg(Helper.build_msg(ProtocolCodes.GET_QUESTION_REQUEST_CODE, self._debug))
        return self.recv_msg(ProtocolCodes.GET_QUESTION_REQUEST_CODE)

    def submit_answer(self, answer_id):
        self.__send_msg(Helper.build_msg(ProtocolCodes.SUBMIT_ANSWER_REQUEST_CODE, self._debug,
                                         {RequestJson.ANSWER_ID: answer_id}))
        return self.recv_msg(ProtocolCodes.SUBMIT_ANSWER_REQUEST_CODE)

    def get_game_results(self):
        self.__send_msg(Helper.build_msg(ProtocolCodes.GET_GAME_RESULTS_REQUEST_CODE, self._debug))
        return self.recv_msg(ProtocolCodes.GET_GAME_RESULTS_REQUEST_CODE)

    def leave_game(self):
        self.__send_msg(Helper.build_msg(ProtocolCodes.LEAVE_GAME_REQUEST_CODE, self._debug))
        return self.recv_msg(ProtocolCodes.LEAVE_GAME_REQUEST_CODE)

    def recv_msg(self, expected_code):
        actual_code = self.client_sock.recv(BYTE_SIZE)
        Helper.assert_result(expected_code, actual_code)
        if self._debug:
            print(f"got code: {actual_code}")
        return self.__recv_buffer()

    def __recv_buffer(self):
        buffer_len = int.from_bytes(self.client_sock.recv(INT_SIZE), byteorder=NETWORK_BYTE_ORDER)
        buffer = None
        if buffer_len != 0:
            buffer = msgpack.unpackb(self.client_sock.recv(buffer_len), raw=False)
        if self._debug:
            Helper.print_debug(f"got: {buffer}")
        return buffer

    def __send_msg(self, msg):
        if self._debug:
            print(f"sending: {msg}")
        self.client_sock.send(msg)

    def __del__(self):
        self.client_sock.close()


class Helper:
    @staticmethod
    def build_msg(code, debug, obj=None):
        if debug:
            Helper.print_debug(f"buffer to send: {obj}")
        if obj:
            buffer = msgpack.packb(obj)
            return code + int.to_bytes(len(buffer), INT_SIZE, NETWORK_BYTE_ORDER) + buffer
        else:
            return code + int.to_bytes(0, INT_SIZE, NETWORK_BYTE_ORDER)

    @staticmethod
    def print_debug(msg):
        print(COLORS.DEBUG_MSG.format(msg))

    @staticmethod
    def create_client(name, password, debug):
        if debug:
            Helper.print_debug(f"creating client (name={name}, pass={password})")
        c = Client(debug)
        ans = c.login(name, password)
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])
        return c

    @staticmethod
    def create_room(client, name, question_time, count, max_users, debug):
        if debug:
            Helper.print_debug(f"Creating Room (name={name}, time={question_time}, count={count}, "
                               f"max_players={max_users})")
        ans = client.create(name, question_time, count, max_users)
        Helper.assert_result(REQUEST_SUCCESSFUL, ans[ResponseJson.STATUS])

    @staticmethod
    def assert_result(expected, actual):
        assert expected == actual, f"Expected: {expected}, Got: {actual}"

    @staticmethod
    def assert_condition(value, predicate):
        assert predicate(value), f"the value {value} failed the condition"

    @staticmethod
    def get_room_id(client, debug):
        ans = client.get_rooms()
        if debug:
            Helper.print_debug(f"target room id: {ans[ResponseJson.ROOMS][0][ResponseJson.ROOM_ID]}")
        return ans[ResponseJson.ROOMS][0][ResponseJson.ROOM_ID]


def main():
    pass


if __name__ == '__main__':
    main()
