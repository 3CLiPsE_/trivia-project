try:
    import colorama  # for convenient text coloring
except ImportError:
    print("output would not be colored. install colorama in order to color output")
    colorama = None
import os

REQUEST_SUCCESSFUL = 200
REQUEST_UNSUCCESSFUL = 201


class Colors:
    def __init__(self):
        if colorama:
            if "PYCHARM_HOSTED" in os.environ:  # if the code is run in pycharm console
                colorama.init(convert=False, strip=False)  # in order to color the text in the pycharm console
            else:
                colorama.init()

            self.NAME = f"{colorama.Fore.LIGHTYELLOW_EX}{{}}{colorama.Fore.RESET}"
            self.FAILED = f"{colorama.Fore.LIGHTRED_EX}failed{colorama.Fore.RESET}"
            self.PASSED = f"{colorama.Fore.LIGHTGREEN_EX}passed{colorama.Fore.RESET}"
            self.DEBUG_MSG = f"{colorama.Fore.LIGHTMAGENTA_EX}DEBUG:{colorama.Fore.RESET} {{}}"
        else:
            self.NAME = "name: {}"
            self.FAILED = " ,failed"
            self.PASSED = " ,passed"
            self.DEBUG_MSG = "DEBUG: {}"


class ProtocolCodes:
    LOGIN_REQUEST_CODE = bytes([24])
    SIGNUP_REQUEST_CODE = bytes([160])

    SIGNOUT_REQUEST_CODE = bytes([51])
    GET_ROOMS_REQUEST_CODE = bytes([216])
    GET_PLAYERS_REQUEST_CODE = bytes([39])
    GET_HIGHSCORES_REQUEST_CODE = bytes([72])
    JOIN_REQUEST_CODE = bytes([77])
    CREATE_REQUEST_CODE = bytes([90])

    LEAVE_ROOM_REQUEST_CODE = bytes([158])
    GET_ROOM_STATE_REQUEST_CODE = bytes([22])
    START_GAME_REQUEST_CODE = bytes([217])
    CLOSE_ROOM_REQUEST_CODE = bytes([116])

    GET_GAME_RESULTS_REQUEST_CODE = bytes([102])
    GET_QUESTION_REQUEST_CODE = bytes([230])
    LEAVE_GAME_REQUEST_CODE = bytes([83])
    SUBMIT_ANSWER_REQUEST_CODE = bytes([254])
    TIME_OUT_RESPONSE_CODE = bytes([145])


class RequestJson:
    USERNAME = "Username"
    PASSWORD = "Password"
    EMAIL_ADDRESS = "Email"
    ROOM_ID = "Room_id"
    ROOM_NAME = "Name"
    MAX_USERS = "Max_users"
    QUESTION_COUNT = "Questions_count"
    QUESTION_TIME = "Time"
    ANSWER_ID = "Answer_Id"


class ResponseJson:
    STATUS = "Status"
    MESSAGE = "Message"
    ROOMS = "Rooms"
    PLAYERS = "Players"
    ROOM_NAME = "Name"
    ROOM_ID = "Id"
    ROOM_TIME = "Question_time"
    ROOM_ACTIVE = "Active"
    ROOM_PLAYERS = "Max_players"
    ROOM_QUESTIONS = "Question_num"
    HIGHSCORES = "Highscores"
    RESULTS = "Results"
    USERNAME = "Username"
    CORRECT_ANSWER_COUNT = "Correct_answer_count"
    WRONG_ANSWER_COUNT = "Wrong_answer_count"
    AVERAGE_ANSWER_COUNT = "Average_answer_count"
    CORRECT_ANSWER_INDEX = "Correct_answer_index"
    ANSWERS = "Answers"
    QUESTION = "Question"


def main():
    pass


if __name__ == '__main__':
    main()
